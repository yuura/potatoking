﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using UnityEngine.SceneManagement;

public class PlayController : MonoBehaviour{
    public GameObject ch;
    public GameObject menu;
    public bool pause;
    public bool finish;
    int i,j=0;
    int num = 0;
    int index;
    String str = "";
    public Image speaker;
    public Sprite on;
    public Sprite off;
    public int cnt;

    void Start()
    {
        cnt = 0;
        speaker = GameObject.Find("VolumeSetting").GetComponent<Image>();
        if (GlobalCall.getbgm() == 3)
            AudioListener.volume = 0.0f;
        else
            AudioListener.volume = 1.0f;
        if (AudioListener.volume == 1.0f)
        {
           speaker.overrideSprite = on;
        }
        else
        {
           speaker.overrideSprite = off;
        }
    }
    void Update()
    {
        if (Application.platform == RuntimePlatform.Android)
        {
            if (Input.GetKey(KeyCode.Home))
            {
                Application.Quit();
            }
            else if (Input.GetKey(KeyCode.Escape))
            {
                SceneManager.LoadScene("Title");
            }
        }
    }
    public void OnVolumeButtonClicked()
    {
        if (AudioListener.volume == 1.0f)
        {
            speaker.overrideSprite = off;
            AudioListener.volume = 0.0f;
        }
        else
        {
           speaker.overrideSprite = on;
            AudioListener.volume = 1.0f;
        }
    }
    public void OnPauseButtonClicked()
    {
        cnt++;
        if (cnt % 2 == 1)
        {
            menu.SetActive(true);
            Time.timeScale = (Time.timeScale > 0.0f) ? 0.0f : 1.0f;
            pause = false;
        }
        else
        {
            menu.SetActive(false);
            Time.timeScale = (Time.timeScale > 0.0f) ? 0.0f : 1.0f;
            pause = true;
        }
    }
}
