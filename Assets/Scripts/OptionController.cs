﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class OptionController : MonoBehaviour
{
    public Sprite mark;
    public Sprite any;
    public Image checkmark;
    public Toggle currenttoggle;
    static bool AudioBegin = false;

    void Start()
    {
        if (GlobalCall.getbgm() == 3)
        {
            currenttoggle.isOn = false;
        }
        else if (GlobalCall.getbgm() == 2)
        {
            currenttoggle.isOn = true;
        }
    }
    void Update()
    {
        if (Application.platform == RuntimePlatform.Android)
        {
            if (Input.GetKey(KeyCode.Escape))
            {
                Application.Quit();
            }
        }
    }

    public void OnBackButtonClicked()
    {
        if (GlobalCall.isprevplay == true)
        {
            SceneManager.LoadScene("Play");
        }
        else if (GlobalCall.isprevplay == false)
        {
            SceneManager.LoadScene("Title");
        }
    }

    public void gameSoundStatusChanged(bool isclick)
    {
        if (currenttoggle.isOn)
        {
            AudioListener.volume = 1.0f;
            GlobalCall.checkbgm(2);
            checkmark.overrideSprite = mark;

        }
        else
        {
            AudioListener.volume = 0.0f;
            GlobalCall.checkbgm(3);
            checkmark.overrideSprite =null;
        }
    }

    public void OnInitializationButtonClicked()
    {
        GlobalCall.setScore(0);
    }
}
