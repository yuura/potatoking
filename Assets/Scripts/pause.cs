﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class pause : MonoBehaviour {

    public GameObject potato_inside;
    public GameObject potato_outside;
    public Sprite play;
    public Sprite pau;
    Image but;

    // Use this for initialization
    void Start () {
        but = GameObject.Find("pause").GetComponent<Image>();
    }
	
	// Update is called once per frame
	void Update () {
	
	}
    public void setPause()
    {
        if (GlobalCall.getLine() == true)
            GlobalCall.setLine(false);
        else
            GlobalCall.setLine(true);
        if(Time.timeScale>0.0f)
        {
            Time.timeScale = 0.0f;
            but.overrideSprite = play;
        }
        else
        {
            Time.timeScale = 1.0f;
            but.overrideSprite = pau;
        }
    }
}
