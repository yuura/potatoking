﻿using UnityEngine;
using System.Collections;

public class LayerDepth1 : MonoBehaviour
{

    static int guiDepth = 1;

    void OnGUI()
    {
        GUI.depth = guiDepth;
    }
}
