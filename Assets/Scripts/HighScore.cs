﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using UnityEngine.SceneManagement;

public class HighScore : MonoBehaviour {

    int i, j = 0;
    int num = 0;
    int cnt;
    int index;
    public static bool[] flag;
    public char[] getnum;
    String str = "";
    public bool pause;
    public static bool finish;
    public GameObject ch;
    public float time;
    public static int score;
    public static int high;
    public static int gamecnt = 0;
    int m;
    int s;
    String mstr;
    String sstr;
    public String highstr;
    public float current;
    string minutestr;
    string secondstr;
    string highsc;
    int sixty;
    float fsixty;
    int minute;
    Text uiText;
    public static int nh;


    void Start () {
        Debug.Log("start");
        uiText = GameObject.Find("CurrentTime").GetComponent<Text>();
        nh = GlobalCall.getScore();
        getnum = new char[9];
        flag = new bool[69]; 
        time = 0;
  
        GlobalCall.setOrigin(GlobalCall.getHigh());


        current = 0;
        minute = 0;
        time = 0;
        if (PlayerPrefs.HasKey("result") == true)
            high = PlayerPrefs.GetInt("hs");
  

        for (i = 0; i < flag.Length; i++)
        {
            flag[i] = false;
        }
    }

 
    void Update()
    {
        time += Time.deltaTime;
        score = Mathf.FloorToInt(time);

        int minute = 0;
        int second = Mathf.FloorToInt(time) % 60;
        minute = Mathf.FloorToInt(time) / 60;
   
        if (minute != 0)
            minutestr = minute.ToString();
        if (second != 0)
            secondstr = second.ToString();
        if (minute < 10)
            minutestr = "0" + minute.ToString();
        if (second < 10)
            secondstr = "0" + second.ToString();
        uiText.text = minutestr + ":" + secondstr;

    }
    public void but()
    {

        str = "";
        getnum = ch.name.ToCharArray();
        str = getnum[6].ToString();
        if (getnum[7] != 'a')
        {
            str += getnum[7].ToString();
        }
        index = Int32.Parse(str);
        Debug.Log("intdex"+index);
        flag[index] = true;
        cnt = 0;
        for (i = 1; i < flag.Length; i++)
        {

            if (flag[i] == true)
            { cnt++; }
            if (cnt >= 60)
            {
                Time.timeScale = 0.0f;
                finish = true;
                savescore();
            }
        }
    }
    public void savescore()
    {
        GlobalCall.setScore(score);
        if (nh == 0)
        {
            high = score;
        }
        else
        {
            if (PlayerPrefs.HasKey("hs") == true)
                high = PlayerPrefs.GetInt("hs");
        }
        if (finish == true)
        {
            if (score <= high)
            {
                PlayerPrefs.SetInt("hs", score);
                high = PlayerPrefs.GetInt("hs");
                PlayerPrefs.Save();
            }
        }

        GlobalCall.setHigh(high);
        Debug.Log("high:" + high);
        Complete();
    }
        
    void Complete()
    {
        SceneManager.LoadScene("Result");
    }
}
