﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public static class GlobalCall
{
    private static bool flag=true;
    public static int currenttime = 0;
    public static int besttime = 0;
    public static float currentBGM = 1.0f;
    public static bool isprevplay;
    public static AudioSource audio;

    public static bool ifstart = false;


    public static void setScore(int score)
    {
        PlayerPrefs.SetInt("reset", score);
        PlayerPrefs.Save();
    }

    public static int getScore()
    {
        int high;
        if (PlayerPrefs.HasKey("reset") == true)
            high = PlayerPrefs.GetInt("reset");
        else
            high = 0;
        return high;
    }

    public static void setHigh(int score)
    {
        PlayerPrefs.SetInt("high", score);
        PlayerPrefs.Save();
    }

    public static int getHigh()
    {
        int high = PlayerPrefs.GetInt("high");
        return high;
    }

    public static void setOrigin(int score)
    {
        PlayerPrefs.SetInt("origin", score);
        PlayerPrefs.Save();
    }

    public static int getOrigin()
    {
        int origin = PlayerPrefs.GetInt("origin");
        return origin;
    }

    public static void setLine(bool check)
    {
        flag = check;
    }

    public static bool getLine()
    {
        return flag;
    }

    public static void checkbgm(int check)
    {
        PlayerPrefs.SetInt("bgm_check", check);
        PlayerPrefs.Save();
    }

    public static int getbgm()
    {
        int bgm_check = PlayerPrefs.GetInt("bgm_check");
        return bgm_check;
    }
}