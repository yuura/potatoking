﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ResultController : MonoBehaviour {
    int score;
    string mstr, sstr;
    int m, s;

    public Text text;
    public GameObject normal;
    public GameObject best;
    


    void Start()
    {
        score = GlobalCall.getScore();
        Debug.Log("score: " + score);
        if (GlobalCall.getOrigin() != GlobalCall.getHigh())
        {
            normal.SetActive(false);
            best.SetActive(true);
        }
        else
        {
            normal.SetActive(true);
            best.SetActive(false);
        }

        score = GlobalCall.getScore();
        m = score / 60;
        s = score % 60;
        if (m != 0)
            mstr = m.ToString();
        if (s != 0)
            sstr = s.ToString();
        if (m < 10)
            mstr = "0" + m.ToString();
        if (s < 10)
            sstr = "0" + s.ToString();
        string resultstr = mstr + ":" + sstr;
        GameObject.Find("Time").GetComponent<Text>().text = resultstr;
        
    }
    void Update()
    {
        if (Application.platform == RuntimePlatform.Android)
        {
            if (Input.GetKey(KeyCode.Escape))
            {
                Application.Quit();
            }
        }
    }
    public void GoToMain()
    {
        SceneManager.LoadScene("Title");
    }

    public void GoToAgain()
    {
        Time.timeScale = 1.0f;
        SceneManager.LoadScene("Play");
    }
}
