﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TitleController : MonoBehaviour {
    public Text record;
    int result;
    string mstr, sstr;
    int m, s;
    static bool AudioBegin = false;
    public AudioClip _playBgm;
    
    void Awake()
    {
        if (GlobalCall.getbgm() == 3)
           AudioListener.volume = 0.0f;
        else if(GlobalCall.getbgm()==2)
           AudioListener.volume = 1.0f;
        
        
        if (AudioListener.volume == 0.0f)
            GlobalCall.checkbgm(3);
        else
            GlobalCall.checkbgm(2);
        
    }
    void Start()
    {
        if (GlobalCall.getScore() == 0)
            result = GlobalCall.getScore();
        else
            result = GlobalCall.getHigh();
        m = result / 60;
        s = result % 60;
        if (m != 0)
            mstr = m.ToString();
        if (s != 0)
            sstr = s.ToString();
        if (m < 10)
            mstr = "0" + m.ToString();
        if (s < 10)
            sstr = "0" + s.ToString();
        string resultstr = mstr + ":" + sstr;
        GameObject.Find("HighScoreTime").GetComponent<Text>().text = resultstr;
    }

    void Update()
    {
        if (Application.platform == RuntimePlatform.Android)
        {
            if (Input.GetKey(KeyCode.Escape)|| Input.GetKey(KeyCode.Escape))
            {
                Application.Quit();
            }
        }
        if (GlobalCall.getbgm() == 3)
           AudioListener.volume = 0.0f;
        else
           AudioListener.volume = 1.0f;
    }


    public void OnStartButtonClicked()
    {
        Time.timeScale = 1.0f;
        Destroy(gameObject);
        SceneManager.LoadScene("Play");
    }

    public void OnOptionButtonClicked()
    {
        SceneManager.LoadScene("Option");
    }
}
