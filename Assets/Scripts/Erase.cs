﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Erase : MonoBehaviour
{
    private LineRenderer line;
    private Vector2 mousePos;
    private Vector2 startPos;
    private Vector2 endPos;
    public float baseWidth;
    public float baseHeight;
    void Start()
    {
    }
    
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (GlobalCall.getLine() == true)
            {
                if (line == null)
                    createLine();
                mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                line.SetPosition(0, mousePos);
                line.SetPosition(1, mousePos);
                startPos = mousePos;
            }
        }
        else if (Input.GetMouseButtonUp(0) && line)
        {
            if (line)
            {
                mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                line.SetPosition(1, mousePos);
                endPos = mousePos;
                line = null;
            }
        }
        else if (Input.GetMouseButton(0))
        {
            if (line)
            {
                mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                line.SetPosition(1, mousePos);

            }
        }
  
    }
    private void createLine()
    {
        line = new GameObject("Line").AddComponent<LineRenderer>();
        line.SetColors(Color.white, Color.white);
        line.material = new Material(Shader.Find("Diffuse"));
        line.SetVertexCount(2);
        
        line.SetWidth(0.2f, 0.2f);
        line.useWorldSpace = true;
    }
}