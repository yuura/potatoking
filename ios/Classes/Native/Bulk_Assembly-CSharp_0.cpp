﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// AudioBGM
struct AudioBGM_t2061041184;
// AutoFade
struct AutoFade_t3786508579;
// System.Object
struct Il2CppObject;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.String
struct String_t;
// AutoFade/<Fade>c__Iterator0
struct U3CFadeU3Ec__Iterator0_t1344610417;
// Check
struct Check_t3582434584;
// UnityEngine.UI.Button
struct Button_t2872111280;
// Clear
struct Clear_t4180584003;
// Erase
struct Erase_t2332422754;
// UnityEngine.EventSystems.EventSystem
struct EventSystem_t3466835263;
// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.LineRenderer
struct LineRenderer_t849157671;
// GameController
struct GameController_t3607102586;
// HighScore
struct HighScore_t2402950694;
// UnityEngine.UI.Text
struct Text_t356221433;
// LayerDepth0
struct LayerDepth0_t2725382046;
// LayerDepth1
struct LayerDepth1_t2725382045;
// OptionController
struct OptionController_t3288053769;
// UnityEngine.AudioSource
struct AudioSource_t1135106623;
// OptionController/<MakeNotice>c__Iterator1
struct U3CMakeNoticeU3Ec__Iterator1_t2390458154;
// PlayController
struct PlayController_t2024502094;
// ResultController
struct ResultController_t1522536863;
// TitleController
struct TitleController_t391985954;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_U3CModuleU3E3783534214.h"
#include "AssemblyU2DCSharp_U3CModuleU3E3783534214MethodDeclarations.h"
#include "AssemblyU2DCSharp_AudioBGM2061041184.h"
#include "AssemblyU2DCSharp_AudioBGM2061041184MethodDeclarations.h"
#include "mscorlib_System_Void1841601450.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972MethodDeclarations.h"
#include "AssemblyU2DCSharp_AutoFade3786508579.h"
#include "AssemblyU2DCSharp_AutoFade3786508579MethodDeclarations.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_String2029220233MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object1021602117MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject1756533147MethodDeclarations.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "UnityEngine_UnityEngine_Object1021602117.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "UnityEngine_UnityEngine_Shader2430389951MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Material193706927MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Mesh1356156583MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Shader2430389951.h"
#include "UnityEngine_UnityEngine_Material193706927.h"
#include "UnityEngine_UnityEngine_Mesh1356156583.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "mscorlib_System_Single2076509932.h"
#include "UnityEngine_UnityEngine_Debug1368543263MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GL1765937205MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color2020392075MethodDeclarations.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_Int322071877448.h"
#include "UnityEngine_UnityEngine_Vector42243707581.h"
#include "UnityEngine_UnityEngine_Vector42243707581MethodDeclarations.h"
#include "AssemblyU2DCSharp_AutoFade_U3CFadeU3Ec__Iterator01344610417MethodDeclarations.h"
#include "AssemblyU2DCSharp_AutoFade_U3CFadeU3Ec__Iterator01344610417.h"
#include "UnityEngine_UnityEngine_Coroutine2299508840.h"
#include "mscorlib_System_Object2689449295MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WaitForEndOfFrame1785723201MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Time31991979MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Mathf2336485820MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Application354826772MethodDeclarations.h"
#include "mscorlib_System_UInt322149682021.h"
#include "UnityEngine_UnityEngine_WaitForEndOfFrame1785723201.h"
#include "mscorlib_System_NotSupportedException1793819818MethodDeclarations.h"
#include "mscorlib_System_NotSupportedException1793819818.h"
#include "AssemblyU2DCSharp_Check3582434584.h"
#include "AssemblyU2DCSharp_Check3582434584MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component3819376471MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Button2872111280.h"
#include "UnityEngine_UnityEngine_Component3819376471.h"
#include "AssemblyU2DCSharp_Clear4180584003.h"
#include "AssemblyU2DCSharp_Clear4180584003MethodDeclarations.h"
#include "AssemblyU2DCSharp_Erase2332422754.h"
#include "AssemblyU2DCSharp_Erase2332422754MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventSyste3466835263.h"
#include "UnityEngine_UnityEngine_Input1785128008MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Camera189460977MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector22243707579MethodDeclarations.h"
#include "UnityEngine_UnityEngine_LineRenderer849157671MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Image2042527209.h"
#include "UnityEngine_UnityEngine_LineRenderer849157671.h"
#include "UnityEngine_UnityEngine_Camera189460977.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_Renderer257310565MethodDeclarations.h"
#include "AssemblyU2DCSharp_GameController3607102586.h"
#include "AssemblyU2DCSharp_GameController3607102586MethodDeclarations.h"
#include "AssemblyU2DCSharp_GlobalCall2994192287.h"
#include "AssemblyU2DCSharp_GlobalCall2994192287MethodDeclarations.h"
#include "UnityEngine_UnityEngine_PlayerPrefs3325146001MethodDeclarations.h"
#include "AssemblyU2DCSharp_HighScore2402950694.h"
#include "AssemblyU2DCSharp_HighScore2402950694MethodDeclarations.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Char3454481338.h"
#include "UnityEngine_UI_UnityEngine_UI_Text356221433.h"
#include "mscorlib_System_Int322071877448MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Text356221433MethodDeclarations.h"
#include "mscorlib_System_Char3454481338MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SceneManagement_SceneManager90660965MethodDeclarations.h"
#include "AssemblyU2DCSharp_LayerDepth02725382046.h"
#include "AssemblyU2DCSharp_LayerDepth02725382046MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUI4082743951MethodDeclarations.h"
#include "AssemblyU2DCSharp_LayerDepth12725382045.h"
#include "AssemblyU2DCSharp_LayerDepth12725382045MethodDeclarations.h"
#include "AssemblyU2DCSharp_OptionController3288053769.h"
#include "AssemblyU2DCSharp_OptionController3288053769MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AudioSource1135106623.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen897193173MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen3863924733MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Toggle3976754468MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Toggle3976754468.h"
#include "UnityEngine_UI_UnityEngine_UI_Toggle_ToggleEvent1896830814.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen897193173.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "UnityEngine_UnityEngine_AudioSource1135106623MethodDeclarations.h"
#include "AssemblyU2DCSharp_OptionController_U3CMakeNoticeU32390458154MethodDeclarations.h"
#include "AssemblyU2DCSharp_OptionController_U3CMakeNoticeU32390458154.h"
#include "UnityEngine_UnityEngine_WaitForSeconds3839502067MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WaitForSeconds3839502067.h"
#include "AssemblyU2DCSharp_PlayController2024502094.h"
#include "AssemblyU2DCSharp_PlayController2024502094MethodDeclarations.h"
#include "AssemblyU2DCSharp_ResultController1522536863.h"
#include "AssemblyU2DCSharp_ResultController1522536863MethodDeclarations.h"
#include "AssemblyU2DCSharp_TitleController391985954.h"
#include "AssemblyU2DCSharp_TitleController391985954MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RuntimePlatform1869584967.h"
#include "UnityEngine_UnityEngine_KeyCode2283395152.h"

// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_AddComponent_TisIl2CppObject_m3813873105_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
#define GameObject_AddComponent_TisIl2CppObject_m3813873105(__this, method) ((  Il2CppObject * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m3813873105_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<AutoFade>()
#define GameObject_AddComponent_TisAutoFade_t3786508579_m2058710109(__this, method) ((  AutoFade_t3786508579 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m3813873105_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m2721246802_gshared (Component_t3819376471 * __this, const MethodInfo* method);
#define Component_GetComponent_TisIl2CppObject_m2721246802(__this, method) ((  Il2CppObject * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m2721246802_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Button>()
#define Component_GetComponent_TisButton_t2872111280_m3412601438(__this, method) ((  Button_t2872111280 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m2721246802_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
#define GameObject_GetComponent_TisIl2CppObject_m2812611596(__this, method) ((  Il2CppObject * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.EventSystems.EventSystem>()
#define GameObject_GetComponent_TisEventSystem_t3466835263_m602036301(__this, method) ((  EventSystem_t3466835263 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Image>()
#define Component_GetComponent_TisImage_t2042527209_m2189462422(__this, method) ((  Image_t2042527209 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m2721246802_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<UnityEngine.LineRenderer>()
#define GameObject_AddComponent_TisLineRenderer_t849157671_m2460104468(__this, method) ((  LineRenderer_t849157671 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m3813873105_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Text>()
#define GameObject_GetComponent_TisText_t356221433_m1217399699(__this, method) ((  Text_t356221433 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.AudioSource>()
#define GameObject_GetComponent_TisAudioSource_t1135106623_m3309832039(__this, method) ((  AudioSource_t1135106623 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void AudioBGM::.ctor()
extern "C"  void AudioBGM__ctor_m1658509437 (AudioBGM_t2061041184 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AutoFade::.ctor()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t AutoFade__ctor_m4174432764_MetadataUsageId;
extern "C"  void AutoFade__ctor_m4174432764 (AutoFade_t3786508579 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AutoFade__ctor_m4174432764_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_m_LevelName_5(L_0);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AutoFade::.cctor()
extern "C"  void AutoFade__cctor_m395142333 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// AutoFade AutoFade::get_Instance()
extern Il2CppClass* AutoFade_t3786508579_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* GameObject_t1756533147_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisAutoFade_t3786508579_m2058710109_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral219619801;
extern const uint32_t AutoFade_get_Instance_m950941996_MetadataUsageId;
extern "C"  AutoFade_t3786508579 * AutoFade_get_Instance_m950941996 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AutoFade_get_Instance_m950941996_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(AutoFade_t3786508579_il2cpp_TypeInfo_var);
		AutoFade_t3786508579 * L_0 = ((AutoFade_t3786508579_StaticFields*)AutoFade_t3786508579_il2cpp_TypeInfo_var->static_fields)->get_m_Instance_2();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0024;
		}
	}
	{
		GameObject_t1756533147 * L_2 = (GameObject_t1756533147 *)il2cpp_codegen_object_new(GameObject_t1756533147_il2cpp_TypeInfo_var);
		GameObject__ctor_m962601984(L_2, _stringLiteral219619801, /*hidden argument*/NULL);
		NullCheck(L_2);
		AutoFade_t3786508579 * L_3 = GameObject_AddComponent_TisAutoFade_t3786508579_m2058710109(L_2, /*hidden argument*/GameObject_AddComponent_TisAutoFade_t3786508579_m2058710109_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(AutoFade_t3786508579_il2cpp_TypeInfo_var);
		((AutoFade_t3786508579_StaticFields*)AutoFade_t3786508579_il2cpp_TypeInfo_var->static_fields)->set_m_Instance_2(L_3);
	}

IL_0024:
	{
		IL2CPP_RUNTIME_CLASS_INIT(AutoFade_t3786508579_il2cpp_TypeInfo_var);
		AutoFade_t3786508579 * L_4 = ((AutoFade_t3786508579_StaticFields*)AutoFade_t3786508579_il2cpp_TypeInfo_var->static_fields)->get_m_Instance_2();
		return L_4;
	}
}
// System.Boolean AutoFade::get_Fading()
extern Il2CppClass* AutoFade_t3786508579_il2cpp_TypeInfo_var;
extern const uint32_t AutoFade_get_Fading_m3921776080_MetadataUsageId;
extern "C"  bool AutoFade_get_Fading_m3921776080 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AutoFade_get_Fading_m3921776080_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(AutoFade_t3786508579_il2cpp_TypeInfo_var);
		AutoFade_t3786508579 * L_0 = AutoFade_get_Instance_m950941996(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = L_0->get_m_Fading_7();
		return L_1;
	}
}
// System.Void AutoFade::Awake()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* AutoFade_t3786508579_il2cpp_TypeInfo_var;
extern Il2CppClass* Material_t193706927_il2cpp_TypeInfo_var;
extern Il2CppClass* Mesh_t1356156583_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4168350146;
extern const uint32_t AutoFade_Awake_m1767706303_MetadataUsageId;
extern "C"  void AutoFade_Awake_m1767706303 (AutoFade_t3786508579 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AutoFade_Awake_m1767706303_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_DontDestroyOnLoad_m2330762974(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(AutoFade_t3786508579_il2cpp_TypeInfo_var);
		((AutoFade_t3786508579_StaticFields*)AutoFade_t3786508579_il2cpp_TypeInfo_var->static_fields)->set_m_Instance_2(__this);
		Shader_t2430389951 * L_0 = Shader_Find_m4179408078(NULL /*static, unused*/, _stringLiteral4168350146, /*hidden argument*/NULL);
		Material_t193706927 * L_1 = (Material_t193706927 *)il2cpp_codegen_object_new(Material_t193706927_il2cpp_TypeInfo_var);
		Material__ctor_m1897560860(L_1, L_0, /*hidden argument*/NULL);
		__this->set_mat_3(L_1);
		Mesh_t1356156583 * L_2 = (Mesh_t1356156583 *)il2cpp_codegen_object_new(Mesh_t1356156583_il2cpp_TypeInfo_var);
		Mesh__ctor_m2975981674(L_2, /*hidden argument*/NULL);
		__this->set_mesh_4(L_2);
		return;
	}
}
// System.Void AutoFade::DrawQuad(UnityEngine.Color,System.Single)
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1752674326;
extern const uint32_t AutoFade_DrawQuad_m1804059946_MetadataUsageId;
extern "C"  void AutoFade_DrawQuad_m1804059946 (AutoFade_t3786508579 * __this, Color_t2020392075  ___aColor0, float ___aAlpha1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AutoFade_DrawQuad_m1804059946_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Material_t193706927 * L_0 = __this->get_mat_3();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_001b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, _stringLiteral1752674326, /*hidden argument*/NULL);
		return;
	}

IL_001b:
	{
		GL_PushMatrix_m1979053131(NULL /*static, unused*/, /*hidden argument*/NULL);
		Material_t193706927 * L_2 = __this->get_mat_3();
		NullCheck(L_2);
		Material_SetPass_m2448940266(L_2, 0, /*hidden argument*/NULL);
		GL_LoadOrtho_m3764403102(NULL /*static, unused*/, /*hidden argument*/NULL);
		GL_Begin_m3874173032(NULL /*static, unused*/, 7, /*hidden argument*/NULL);
		float L_3 = (&___aColor0)->get_r_0();
		float L_4 = (&___aColor0)->get_g_1();
		float L_5 = (&___aColor0)->get_b_2();
		float L_6 = ___aAlpha1;
		Vector4_t2243707581  L_7;
		memset(&L_7, 0, sizeof(L_7));
		Vector4__ctor_m1222289168(&L_7, L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
		Color_t2020392075  L_8 = Color_op_Implicit_m983896778(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		GL_Color_m3254827061(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		GL_Vertex3_m3998822656(NULL /*static, unused*/, (0.0f), (0.0f), (-1.0f), /*hidden argument*/NULL);
		GL_Vertex3_m3998822656(NULL /*static, unused*/, (0.0f), (1.0f), (-1.0f), /*hidden argument*/NULL);
		GL_Vertex3_m3998822656(NULL /*static, unused*/, (1.0f), (1.0f), (-1.0f), /*hidden argument*/NULL);
		GL_Vertex3_m3998822656(NULL /*static, unused*/, (1.0f), (0.0f), (-1.0f), /*hidden argument*/NULL);
		GL_End_m2374230645(NULL /*static, unused*/, /*hidden argument*/NULL);
		GL_PopMatrix_m856033754(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator AutoFade::Fade(System.Single,System.Single,UnityEngine.Color)
extern Il2CppClass* U3CFadeU3Ec__Iterator0_t1344610417_il2cpp_TypeInfo_var;
extern const uint32_t AutoFade_Fade_m3240942000_MetadataUsageId;
extern "C"  Il2CppObject * AutoFade_Fade_m3240942000 (AutoFade_t3786508579 * __this, float ___aFadeOutTime0, float ___aFadeInTime1, Color_t2020392075  ___aColor2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AutoFade_Fade_m3240942000_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CFadeU3Ec__Iterator0_t1344610417 * V_0 = NULL;
	{
		U3CFadeU3Ec__Iterator0_t1344610417 * L_0 = (U3CFadeU3Ec__Iterator0_t1344610417 *)il2cpp_codegen_object_new(U3CFadeU3Ec__Iterator0_t1344610417_il2cpp_TypeInfo_var);
		U3CFadeU3Ec__Iterator0__ctor_m1473650912(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CFadeU3Ec__Iterator0_t1344610417 * L_1 = V_0;
		float L_2 = ___aFadeOutTime0;
		NullCheck(L_1);
		L_1->set_aFadeOutTime_1(L_2);
		U3CFadeU3Ec__Iterator0_t1344610417 * L_3 = V_0;
		Color_t2020392075  L_4 = ___aColor2;
		NullCheck(L_3);
		L_3->set_aColor_2(L_4);
		U3CFadeU3Ec__Iterator0_t1344610417 * L_5 = V_0;
		float L_6 = ___aFadeInTime1;
		NullCheck(L_5);
		L_5->set_aFadeInTime_3(L_6);
		U3CFadeU3Ec__Iterator0_t1344610417 * L_7 = V_0;
		float L_8 = ___aFadeOutTime0;
		NullCheck(L_7);
		L_7->set_U3CU24U3EaFadeOutTime_6(L_8);
		U3CFadeU3Ec__Iterator0_t1344610417 * L_9 = V_0;
		Color_t2020392075  L_10 = ___aColor2;
		NullCheck(L_9);
		L_9->set_U3CU24U3EaColor_7(L_10);
		U3CFadeU3Ec__Iterator0_t1344610417 * L_11 = V_0;
		float L_12 = ___aFadeInTime1;
		NullCheck(L_11);
		L_11->set_U3CU24U3EaFadeInTime_8(L_12);
		U3CFadeU3Ec__Iterator0_t1344610417 * L_13 = V_0;
		NullCheck(L_13);
		L_13->set_U3CU3Ef__this_9(__this);
		U3CFadeU3Ec__Iterator0_t1344610417 * L_14 = V_0;
		return L_14;
	}
}
// System.Void AutoFade::StartFade(System.Single,System.Single,UnityEngine.Color)
extern "C"  void AutoFade_StartFade_m257239734 (AutoFade_t3786508579 * __this, float ___aFadeOutTime0, float ___aFadeInTime1, Color_t2020392075  ___aColor2, const MethodInfo* method)
{
	{
		__this->set_m_Fading_7((bool)1);
		float L_0 = ___aFadeOutTime0;
		float L_1 = ___aFadeInTime1;
		Color_t2020392075  L_2 = ___aColor2;
		Il2CppObject * L_3 = AutoFade_Fade_m3240942000(__this, L_0, L_1, L_2, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m2470621050(__this, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AutoFade::LoadLevel(System.String,System.Single,System.Single,UnityEngine.Color)
extern Il2CppClass* AutoFade_t3786508579_il2cpp_TypeInfo_var;
extern const uint32_t AutoFade_LoadLevel_m1709620906_MetadataUsageId;
extern "C"  void AutoFade_LoadLevel_m1709620906 (Il2CppObject * __this /* static, unused */, String_t* ___aLevelName0, float ___aFadeOutTime1, float ___aFadeInTime2, Color_t2020392075  ___aColor3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AutoFade_LoadLevel_m1709620906_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(AutoFade_t3786508579_il2cpp_TypeInfo_var);
		bool L_0 = AutoFade_get_Fading_m3921776080(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_000b;
		}
	}
	{
		return;
	}

IL_000b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(AutoFade_t3786508579_il2cpp_TypeInfo_var);
		AutoFade_t3786508579 * L_1 = AutoFade_get_Instance_m950941996(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_2 = ___aLevelName0;
		NullCheck(L_1);
		L_1->set_m_LevelName_5(L_2);
		AutoFade_t3786508579 * L_3 = AutoFade_get_Instance_m950941996(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_4 = ___aFadeOutTime1;
		float L_5 = ___aFadeInTime2;
		Color_t2020392075  L_6 = ___aColor3;
		NullCheck(L_3);
		AutoFade_StartFade_m257239734(L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AutoFade::LoadLevel(System.Int32,System.Single,System.Single,UnityEngine.Color)
extern Il2CppClass* AutoFade_t3786508579_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t AutoFade_LoadLevel_m2302776913_MetadataUsageId;
extern "C"  void AutoFade_LoadLevel_m2302776913 (Il2CppObject * __this /* static, unused */, int32_t ___aLevelIndex0, float ___aFadeOutTime1, float ___aFadeInTime2, Color_t2020392075  ___aColor3, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AutoFade_LoadLevel_m2302776913_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(AutoFade_t3786508579_il2cpp_TypeInfo_var);
		bool L_0 = AutoFade_get_Fading_m3921776080(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_000b;
		}
	}
	{
		return;
	}

IL_000b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(AutoFade_t3786508579_il2cpp_TypeInfo_var);
		AutoFade_t3786508579 * L_1 = AutoFade_get_Instance_m950941996(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_1);
		L_1->set_m_LevelName_5(L_2);
		AutoFade_t3786508579 * L_3 = AutoFade_get_Instance_m950941996(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_4 = ___aLevelIndex0;
		NullCheck(L_3);
		L_3->set_m_LevelIndex_6(L_4);
		AutoFade_t3786508579 * L_5 = AutoFade_get_Instance_m950941996(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_6 = ___aFadeOutTime1;
		float L_7 = ___aFadeInTime2;
		Color_t2020392075  L_8 = ___aColor3;
		NullCheck(L_5);
		AutoFade_StartFade_m257239734(L_5, L_6, L_7, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AutoFade/<Fade>c__Iterator0::.ctor()
extern "C"  void U3CFadeU3Ec__Iterator0__ctor_m1473650912 (U3CFadeU3Ec__Iterator0_t1344610417 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object AutoFade/<Fade>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CFadeU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m2526559810 (U3CFadeU3Ec__Iterator0_t1344610417 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_5();
		return L_0;
	}
}
// System.Object AutoFade/<Fade>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CFadeU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m30931610 (U3CFadeU3Ec__Iterator0_t1344610417 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_5();
		return L_0;
	}
}
// System.Boolean AutoFade/<Fade>c__Iterator0::MoveNext()
extern Il2CppClass* WaitForEndOfFrame_t1785723201_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2054540110;
extern const uint32_t U3CFadeU3Ec__Iterator0_MoveNext_m2569800596_MetadataUsageId;
extern "C"  bool U3CFadeU3Ec__Iterator0_MoveNext_m2569800596 (U3CFadeU3Ec__Iterator0_t1344610417 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CFadeU3Ec__Iterator0_MoveNext_m2569800596_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_4();
		V_0 = L_0;
		__this->set_U24PC_4((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0025;
		}
		if (L_1 == 1)
		{
			goto IL_004c;
		}
		if (L_1 == 2)
		{
			goto IL_0106;
		}
	}
	{
		goto IL_0178;
	}

IL_0025:
	{
		__this->set_U3CtU3E__0_0((0.0f));
		goto IL_009b;
	}

IL_0035:
	{
		WaitForEndOfFrame_t1785723201 * L_2 = (WaitForEndOfFrame_t1785723201 *)il2cpp_codegen_object_new(WaitForEndOfFrame_t1785723201_il2cpp_TypeInfo_var);
		WaitForEndOfFrame__ctor_m3062480170(L_2, /*hidden argument*/NULL);
		__this->set_U24current_5(L_2);
		__this->set_U24PC_4(1);
		goto IL_017a;
	}

IL_004c:
	{
		float L_3 = __this->get_U3CtU3E__0_0();
		float L_4 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_5 = __this->get_aFadeOutTime_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_6 = Mathf_Clamp01_m3888954684(NULL /*static, unused*/, ((float)((float)L_3+(float)((float)((float)L_4/(float)L_5)))), /*hidden argument*/NULL);
		__this->set_U3CtU3E__0_0(L_6);
		AutoFade_t3786508579 * L_7 = __this->get_U3CU3Ef__this_9();
		Color_t2020392075  L_8 = __this->get_aColor_2();
		float L_9 = __this->get_U3CtU3E__0_0();
		NullCheck(L_7);
		AutoFade_DrawQuad_m1804059946(L_7, L_8, L_9, /*hidden argument*/NULL);
		float L_10 = __this->get_U3CtU3E__0_0();
		float L_11 = L_10;
		Il2CppObject * L_12 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_11);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral2054540110, L_12, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
	}

IL_009b:
	{
		float L_14 = __this->get_U3CtU3E__0_0();
		if ((((float)L_14) < ((float)(1.0f))))
		{
			goto IL_0035;
		}
	}
	{
		AutoFade_t3786508579 * L_15 = __this->get_U3CU3Ef__this_9();
		NullCheck(L_15);
		String_t* L_16 = L_15->get_m_LevelName_5();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_17 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		bool L_18 = String_op_Inequality_m304203149(NULL /*static, unused*/, L_16, L_17, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_00da;
		}
	}
	{
		AutoFade_t3786508579 * L_19 = __this->get_U3CU3Ef__this_9();
		NullCheck(L_19);
		String_t* L_20 = L_19->get_m_LevelName_5();
		Application_LoadLevel_m393995325(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		goto IL_00ea;
	}

IL_00da:
	{
		AutoFade_t3786508579 * L_21 = __this->get_U3CU3Ef__this_9();
		NullCheck(L_21);
		int32_t L_22 = L_21->get_m_LevelIndex_6();
		Application_LoadLevel_m3450161284(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
	}

IL_00ea:
	{
		goto IL_0155;
	}

IL_00ef:
	{
		WaitForEndOfFrame_t1785723201 * L_23 = (WaitForEndOfFrame_t1785723201 *)il2cpp_codegen_object_new(WaitForEndOfFrame_t1785723201_il2cpp_TypeInfo_var);
		WaitForEndOfFrame__ctor_m3062480170(L_23, /*hidden argument*/NULL);
		__this->set_U24current_5(L_23);
		__this->set_U24PC_4(2);
		goto IL_017a;
	}

IL_0106:
	{
		float L_24 = __this->get_U3CtU3E__0_0();
		float L_25 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_26 = __this->get_aFadeInTime_3();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_27 = Mathf_Clamp01_m3888954684(NULL /*static, unused*/, ((float)((float)L_24-(float)((float)((float)L_25/(float)L_26)))), /*hidden argument*/NULL);
		__this->set_U3CtU3E__0_0(L_27);
		AutoFade_t3786508579 * L_28 = __this->get_U3CU3Ef__this_9();
		Color_t2020392075  L_29 = __this->get_aColor_2();
		float L_30 = __this->get_U3CtU3E__0_0();
		NullCheck(L_28);
		AutoFade_DrawQuad_m1804059946(L_28, L_29, L_30, /*hidden argument*/NULL);
		float L_31 = __this->get_U3CtU3E__0_0();
		float L_32 = L_31;
		Il2CppObject * L_33 = Box(Single_t2076509932_il2cpp_TypeInfo_var, &L_32);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_34 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral2054540110, L_33, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_34, /*hidden argument*/NULL);
	}

IL_0155:
	{
		float L_35 = __this->get_U3CtU3E__0_0();
		if ((((float)L_35) > ((float)(0.0f))))
		{
			goto IL_00ef;
		}
	}
	{
		AutoFade_t3786508579 * L_36 = __this->get_U3CU3Ef__this_9();
		NullCheck(L_36);
		L_36->set_m_Fading_7((bool)0);
		__this->set_U24PC_4((-1));
	}

IL_0178:
	{
		return (bool)0;
	}

IL_017a:
	{
		return (bool)1;
	}
	// Dead block : IL_017c: ldloc.1
}
// System.Void AutoFade/<Fade>c__Iterator0::Dispose()
extern "C"  void U3CFadeU3Ec__Iterator0_Dispose_m3904583779 (U3CFadeU3Ec__Iterator0_t1344610417 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_4((-1));
		return;
	}
}
// System.Void AutoFade/<Fade>c__Iterator0::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CFadeU3Ec__Iterator0_Reset_m331973909_MetadataUsageId;
extern "C"  void U3CFadeU3Ec__Iterator0_Reset_m331973909 (U3CFadeU3Ec__Iterator0_t1344610417 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CFadeU3Ec__Iterator0_Reset_m331973909_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void Check::.ctor()
extern "C"  void Check__ctor_m2578362865 (Check_t3582434584 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Check::Start()
extern const MethodInfo* Component_GetComponent_TisButton_t2872111280_m3412601438_MethodInfo_var;
extern const uint32_t Check_Start_m1734623497_MetadataUsageId;
extern "C"  void Check_Start_m1734623497 (Check_t3582434584 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Check_Start_m1734623497_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Button_t2872111280 * L_0 = Component_GetComponent_TisButton_t2872111280_m3412601438(__this, /*hidden argument*/Component_GetComponent_TisButton_t2872111280_m3412601438_MethodInfo_var);
		__this->set_b1_2(L_0);
		return;
	}
}
// System.Void Check::Update()
extern "C"  void Check_Update_m392427046 (Check_t3582434584 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Check::onClick()
extern "C"  void Check_onClick_m1465768360 (Check_t3582434584 * __this, const MethodInfo* method)
{
	float G_B3_0 = 0.0f;
	{
		float L_0 = Time_get_timeScale_m3151482970(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((float)L_0) > ((float)(0.0f)))))
		{
			goto IL_0019;
		}
	}
	{
		G_B3_0 = (0.0f);
		goto IL_001e;
	}

IL_0019:
	{
		G_B3_0 = (1.0f);
	}

IL_001e:
	{
		Time_set_timeScale_m2194722837(NULL /*static, unused*/, G_B3_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Clear::.ctor()
extern "C"  void Clear__ctor_m3273534932 (Clear_t4180584003 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Clear::Start()
extern "C"  void Clear_Start_m405010312 (Clear_t4180584003 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Clear::Update()
extern "C"  void Clear_Update_m1741830927 (Clear_t4180584003 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Erase::.ctor()
extern "C"  void Erase__ctor_m2756617947 (Erase_t2332422754 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Erase::Start()
extern const MethodInfo* GameObject_GetComponent_TisEventSystem_t3466835263_m602036301_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3628703177;
extern const uint32_t Erase_Start_m823459367_MetadataUsageId;
extern "C"  void Erase_Start_m823459367 (Erase_t2332422754 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Erase_Start_m823459367_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral3628703177, /*hidden argument*/NULL);
		NullCheck(L_0);
		EventSystem_t3466835263 * L_1 = GameObject_GetComponent_TisEventSystem_t3466835263_m602036301(L_0, /*hidden argument*/GameObject_GetComponent_TisEventSystem_t3466835263_m602036301_MethodInfo_var);
		__this->set__eventSystem_2(L_1);
		__this->set_count_5(0);
		return;
	}
}
// System.Void Erase::Update()
extern Il2CppClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisImage_t2042527209_m2189462422_MethodInfo_var;
extern const uint32_t Erase_Update_m4159129776_MetadataUsageId;
extern "C"  void Erase_Update_m4159129776 (Erase_t2332422754 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Erase_Update_m4159129776_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Image_t2042527209 * L_0 = Component_GetComponent_TisImage_t2042527209_m2189462422(__this, /*hidden argument*/Component_GetComponent_TisImage_t2042527209_m2189462422_MethodInfo_var);
		__this->set_potato_9(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_1 = Input_GetMouseButtonDown_m47917805(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0087;
		}
	}
	{
		LineRenderer_t849157671 * L_2 = __this->get_line_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_2, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_002e;
		}
	}
	{
		Erase_createLine_m3264391977(__this, /*hidden argument*/NULL);
	}

IL_002e:
	{
		Camera_t189460977 * L_4 = Camera_get_main_m475173995(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		Vector3_t2243707580  L_5 = Input_get_mousePosition_m146923508(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		Vector3_t2243707580  L_6 = Camera_ScreenToWorldPoint_m929392728(L_4, L_5, /*hidden argument*/NULL);
		Vector2_t2243707579  L_7 = Vector2_op_Implicit_m1064335535(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		__this->set_mousePos_6(L_7);
		LineRenderer_t849157671 * L_8 = __this->get_line_4();
		Vector2_t2243707579  L_9 = __this->get_mousePos_6();
		Vector3_t2243707580  L_10 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		NullCheck(L_8);
		LineRenderer_SetPosition_m4048451705(L_8, 0, L_10, /*hidden argument*/NULL);
		LineRenderer_t849157671 * L_11 = __this->get_line_4();
		Vector2_t2243707579  L_12 = __this->get_mousePos_6();
		Vector3_t2243707580  L_13 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		NullCheck(L_11);
		LineRenderer_SetPosition_m4048451705(L_11, 1, L_13, /*hidden argument*/NULL);
		Vector2_t2243707579  L_14 = __this->get_mousePos_6();
		__this->set_startPos_7(L_14);
		goto IL_0147;
	}

IL_0087:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_15 = Input_GetMouseButtonUp_m1275967966(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_00fb;
		}
	}
	{
		LineRenderer_t849157671 * L_16 = __this->get_line_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_17 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_00fb;
		}
	}
	{
		LineRenderer_t849157671 * L_18 = __this->get_line_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_19 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_00f6;
		}
	}
	{
		Camera_t189460977 * L_20 = Camera_get_main_m475173995(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		Vector3_t2243707580  L_21 = Input_get_mousePosition_m146923508(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_20);
		Vector3_t2243707580  L_22 = Camera_ScreenToWorldPoint_m929392728(L_20, L_21, /*hidden argument*/NULL);
		Vector2_t2243707579  L_23 = Vector2_op_Implicit_m1064335535(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
		__this->set_mousePos_6(L_23);
		LineRenderer_t849157671 * L_24 = __this->get_line_4();
		Vector2_t2243707579  L_25 = __this->get_mousePos_6();
		Vector3_t2243707580  L_26 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, L_25, /*hidden argument*/NULL);
		NullCheck(L_24);
		LineRenderer_SetPosition_m4048451705(L_24, 1, L_26, /*hidden argument*/NULL);
		Vector2_t2243707579  L_27 = __this->get_mousePos_6();
		__this->set_endPos_8(L_27);
		__this->set_line_4((LineRenderer_t849157671 *)NULL);
	}

IL_00f6:
	{
		goto IL_0147;
	}

IL_00fb:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_28 = Input_GetMouseButton_m464100923(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		if (!L_28)
		{
			goto IL_0147;
		}
	}
	{
		LineRenderer_t849157671 * L_29 = __this->get_line_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_30 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_29, /*hidden argument*/NULL);
		if (!L_30)
		{
			goto IL_0147;
		}
	}
	{
		Camera_t189460977 * L_31 = Camera_get_main_m475173995(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		Vector3_t2243707580  L_32 = Input_get_mousePosition_m146923508(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_31);
		Vector3_t2243707580  L_33 = Camera_ScreenToWorldPoint_m929392728(L_31, L_32, /*hidden argument*/NULL);
		Vector2_t2243707579  L_34 = Vector2_op_Implicit_m1064335535(NULL /*static, unused*/, L_33, /*hidden argument*/NULL);
		__this->set_mousePos_6(L_34);
		LineRenderer_t849157671 * L_35 = __this->get_line_4();
		Vector2_t2243707579  L_36 = __this->get_mousePos_6();
		Vector3_t2243707580  L_37 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, L_36, /*hidden argument*/NULL);
		NullCheck(L_35);
		LineRenderer_SetPosition_m4048451705(L_35, 1, L_37, /*hidden argument*/NULL);
	}

IL_0147:
	{
		LineRenderer_t849157671 * L_38 = __this->get_line_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_39 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_38, /*hidden argument*/NULL);
		if (!L_39)
		{
			goto IL_0193;
		}
	}
	{
		Image_t2042527209 * L_40 = __this->get_potato_9();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_41 = Object_op_Implicit_m2856731593(NULL /*static, unused*/, L_40, /*hidden argument*/NULL);
		if (!L_41)
		{
			goto IL_0193;
		}
	}
	{
		bool L_42 = __this->get_flag_3();
		if (L_42)
		{
			goto IL_017e;
		}
	}
	{
		__this->set_flag_3((bool)1);
		goto IL_0185;
	}

IL_017e:
	{
		__this->set_flag_3((bool)0);
	}

IL_0185:
	{
		int32_t L_43 = __this->get_count_5();
		__this->set_count_5(((int32_t)((int32_t)L_43+(int32_t)1)));
	}

IL_0193:
	{
		return;
	}
}
// System.Void Erase::createLine()
extern Il2CppClass* GameObject_t1756533147_il2cpp_TypeInfo_var;
extern Il2CppClass* Material_t193706927_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisLineRenderer_t849157671_m2460104468_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3457520020;
extern Il2CppCodeGenString* _stringLiteral2166711796;
extern const uint32_t Erase_createLine_m3264391977_MetadataUsageId;
extern "C"  void Erase_createLine_m3264391977 (Erase_t2332422754 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Erase_createLine_m3264391977_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = (GameObject_t1756533147 *)il2cpp_codegen_object_new(GameObject_t1756533147_il2cpp_TypeInfo_var);
		GameObject__ctor_m962601984(L_0, _stringLiteral3457520020, /*hidden argument*/NULL);
		NullCheck(L_0);
		LineRenderer_t849157671 * L_1 = GameObject_AddComponent_TisLineRenderer_t849157671_m2460104468(L_0, /*hidden argument*/GameObject_AddComponent_TisLineRenderer_t849157671_m2460104468_MethodInfo_var);
		__this->set_line_4(L_1);
		LineRenderer_t849157671 * L_2 = __this->get_line_4();
		Color_t2020392075  L_3 = Color_get_white_m3987539815(NULL /*static, unused*/, /*hidden argument*/NULL);
		Color_t2020392075  L_4 = Color_get_white_m3987539815(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		LineRenderer_SetColors_m3539990388(L_2, L_3, L_4, /*hidden argument*/NULL);
		LineRenderer_t849157671 * L_5 = __this->get_line_4();
		Shader_t2430389951 * L_6 = Shader_Find_m4179408078(NULL /*static, unused*/, _stringLiteral2166711796, /*hidden argument*/NULL);
		Material_t193706927 * L_7 = (Material_t193706927 *)il2cpp_codegen_object_new(Material_t193706927_il2cpp_TypeInfo_var);
		Material__ctor_m1897560860(L_7, L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		Renderer_set_material_m1053097112(L_5, L_7, /*hidden argument*/NULL);
		LineRenderer_t849157671 * L_8 = __this->get_line_4();
		NullCheck(L_8);
		LineRenderer_SetVertexCount_m2685086340(L_8, 2, /*hidden argument*/NULL);
		LineRenderer_t849157671 * L_9 = __this->get_line_4();
		NullCheck(L_9);
		LineRenderer_SetWidth_m178226110(L_9, (0.3f), (0.3f), /*hidden argument*/NULL);
		LineRenderer_t849157671 * L_10 = __this->get_line_4();
		NullCheck(L_10);
		LineRenderer_set_useWorldSpace_m3177357953(L_10, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameController::.ctor()
extern "C"  void GameController__ctor_m1439649957 (GameController_t3607102586 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameController::Start()
extern "C"  void GameController_Start_m239487205 (GameController_t3607102586 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void GameController::Update()
extern "C"  void GameController_Update_m1556003900 (GameController_t3607102586 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void GlobalCall::.cctor()
extern Il2CppClass* GlobalCall_t2994192287_il2cpp_TypeInfo_var;
extern const uint32_t GlobalCall__cctor_m1051184633_MetadataUsageId;
extern "C"  void GlobalCall__cctor_m1051184633 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GlobalCall__cctor_m1051184633_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((GlobalCall_t2994192287_StaticFields*)GlobalCall_t2994192287_il2cpp_TypeInfo_var->static_fields)->set_currentBGM_2((1.0f));
		return;
	}
}
// System.Void GlobalCall::setScore(System.Int32)
extern Il2CppCodeGenString* _stringLiteral1594909257;
extern const uint32_t GlobalCall_setScore_m1324356053_MetadataUsageId;
extern "C"  void GlobalCall_setScore_m1324356053 (Il2CppObject * __this /* static, unused */, int32_t ___score0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GlobalCall_setScore_m1324356053_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___score0;
		PlayerPrefs_SetInt_m3351928596(NULL /*static, unused*/, _stringLiteral1594909257, L_0, /*hidden argument*/NULL);
		PlayerPrefs_Save_m212854761(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 GlobalCall::getScore()
extern Il2CppCodeGenString* _stringLiteral1594909257;
extern const uint32_t GlobalCall_getScore_m1953950224_MetadataUsageId;
extern "C"  int32_t GlobalCall_getScore_m1953950224 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GlobalCall_getScore_m1953950224_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		bool L_0 = PlayerPrefs_HasKey_m1212656251(NULL /*static, unused*/, _stringLiteral1594909257, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_001f;
		}
	}
	{
		int32_t L_1 = PlayerPrefs_GetInt_m2889062785(NULL /*static, unused*/, _stringLiteral1594909257, /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_0021;
	}

IL_001f:
	{
		V_0 = 0;
	}

IL_0021:
	{
		int32_t L_2 = V_0;
		return L_2;
	}
}
// System.Void GlobalCall::setHigh(System.Int32)
extern Il2CppCodeGenString* _stringLiteral4217033790;
extern const uint32_t GlobalCall_setHigh_m735216011_MetadataUsageId;
extern "C"  void GlobalCall_setHigh_m735216011 (Il2CppObject * __this /* static, unused */, int32_t ___score0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GlobalCall_setHigh_m735216011_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___score0;
		PlayerPrefs_SetInt_m3351928596(NULL /*static, unused*/, _stringLiteral4217033790, L_0, /*hidden argument*/NULL);
		PlayerPrefs_Save_m212854761(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 GlobalCall::getHigh()
extern Il2CppCodeGenString* _stringLiteral4217033790;
extern const uint32_t GlobalCall_getHigh_m3144168644_MetadataUsageId;
extern "C"  int32_t GlobalCall_getHigh_m3144168644 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GlobalCall_getHigh_m3144168644_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = PlayerPrefs_GetInt_m2889062785(NULL /*static, unused*/, _stringLiteral4217033790, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Void GlobalCall::setOrigin(System.Int32)
extern Il2CppCodeGenString* _stringLiteral3886432928;
extern const uint32_t GlobalCall_setOrigin_m1076086725_MetadataUsageId;
extern "C"  void GlobalCall_setOrigin_m1076086725 (Il2CppObject * __this /* static, unused */, int32_t ___score0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GlobalCall_setOrigin_m1076086725_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___score0;
		PlayerPrefs_SetInt_m3351928596(NULL /*static, unused*/, _stringLiteral3886432928, L_0, /*hidden argument*/NULL);
		PlayerPrefs_Save_m212854761(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 GlobalCall::getOrigin()
extern Il2CppCodeGenString* _stringLiteral3886432928;
extern const uint32_t GlobalCall_getOrigin_m2405469574_MetadataUsageId;
extern "C"  int32_t GlobalCall_getOrigin_m2405469574 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GlobalCall_getOrigin_m2405469574_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = PlayerPrefs_GetInt_m2889062785(NULL /*static, unused*/, _stringLiteral3886432928, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Void HighScore::.ctor()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t HighScore__ctor_m1595199985_MetadataUsageId;
extern "C"  void HighScore__ctor_m1595199985 (HighScore_t2402950694 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (HighScore__ctor_m1595199985_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_str_9(L_0);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HighScore::.cctor()
extern "C"  void HighScore__cctor_m2596394000 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void HighScore::Start()
extern Il2CppClass* GlobalCall_t2994192287_il2cpp_TypeInfo_var;
extern Il2CppClass* HighScore_t2402950694_il2cpp_TypeInfo_var;
extern Il2CppClass* CharU5BU5D_t1328083999_il2cpp_TypeInfo_var;
extern Il2CppClass* BooleanU5BU5D_t3568034315_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1624159533;
extern Il2CppCodeGenString* _stringLiteral3110053179;
extern const uint32_t HighScore_Start_m3561044161_MetadataUsageId;
extern "C"  void HighScore_Start_m3561044161 (HighScore_t2402950694 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (HighScore_Start_m3561044161_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GlobalCall_t2994192287_il2cpp_TypeInfo_var);
		int32_t L_0 = GlobalCall_getScore_m1953950224(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(HighScore_t2402950694_il2cpp_TypeInfo_var);
		((HighScore_t2402950694_StaticFields*)HighScore_t2402950694_il2cpp_TypeInfo_var->static_fields)->set_nh_30(L_0);
		__this->set_getnum_8(((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)((int32_t)9))));
		((HighScore_t2402950694_StaticFields*)HighScore_t2402950694_il2cpp_TypeInfo_var->static_fields)->set_flag_7(((BooleanU5BU5D_t3568034315*)SZArrayNew(BooleanU5BU5D_t3568034315_il2cpp_TypeInfo_var, (uint32_t)((int32_t)100))));
		__this->set_time_13((0.0f));
		int32_t L_1 = GlobalCall_getHigh_m3144168644(NULL /*static, unused*/, /*hidden argument*/NULL);
		GlobalCall_setOrigin_m1076086725(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		__this->set_current_22((0.0f));
		__this->set_minute_28(0);
		__this->set_time_13((0.0f));
		bool L_2 = PlayerPrefs_HasKey_m1212656251(NULL /*static, unused*/, _stringLiteral1624159533, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0073;
		}
	}
	{
		int32_t L_3 = PlayerPrefs_GetInt_m2889062785(NULL /*static, unused*/, _stringLiteral3110053179, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(HighScore_t2402950694_il2cpp_TypeInfo_var);
		((HighScore_t2402950694_StaticFields*)HighScore_t2402950694_il2cpp_TypeInfo_var->static_fields)->set_high_15(L_3);
	}

IL_0073:
	{
		__this->set_i_2(0);
		goto IL_009a;
	}

IL_007f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(HighScore_t2402950694_il2cpp_TypeInfo_var);
		BooleanU5BU5D_t3568034315* L_4 = ((HighScore_t2402950694_StaticFields*)HighScore_t2402950694_il2cpp_TypeInfo_var->static_fields)->get_flag_7();
		int32_t L_5 = __this->get_i_2();
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (bool)0);
		int32_t L_6 = __this->get_i_2();
		__this->set_i_2(((int32_t)((int32_t)L_6+(int32_t)1)));
	}

IL_009a:
	{
		int32_t L_7 = __this->get_i_2();
		IL2CPP_RUNTIME_CLASS_INIT(HighScore_t2402950694_il2cpp_TypeInfo_var);
		BooleanU5BU5D_t3568034315* L_8 = ((HighScore_t2402950694_StaticFields*)HighScore_t2402950694_il2cpp_TypeInfo_var->static_fields)->get_flag_7();
		NullCheck(L_8);
		if ((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_8)->max_length)))))))
		{
			goto IL_007f;
		}
	}
	{
		return;
	}
}
// System.Void HighScore::Update()
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern Il2CppClass* HighScore_t2402950694_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisText_t356221433_m1217399699_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3315083570;
extern Il2CppCodeGenString* _stringLiteral372029326;
extern Il2CppCodeGenString* _stringLiteral372029336;
extern const uint32_t HighScore_Update_m2360423300_MetadataUsageId;
extern "C"  void HighScore_Update_m2360423300 (HighScore_t2402950694 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (HighScore_Update_m2360423300_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		float L_0 = __this->get_time_13();
		float L_1 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_time_13(((float)((float)L_0+(float)L_1)));
		float L_2 = __this->get_time_13();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		int32_t L_3 = Mathf_FloorToInt_m4005035722(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(HighScore_t2402950694_il2cpp_TypeInfo_var);
		((HighScore_t2402950694_StaticFields*)HighScore_t2402950694_il2cpp_TypeInfo_var->static_fields)->set_score_14(L_3);
		GameObject_t1756533147 * L_4 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral3315083570, /*hidden argument*/NULL);
		NullCheck(L_4);
		Text_t356221433 * L_5 = GameObject_GetComponent_TisText_t356221433_m1217399699(L_4, /*hidden argument*/GameObject_GetComponent_TisText_t356221433_m1217399699_MethodInfo_var);
		__this->set_uiText_29(L_5);
		V_0 = 0;
		float L_6 = __this->get_time_13();
		int32_t L_7 = Mathf_FloorToInt_m4005035722(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		V_1 = ((int32_t)((int32_t)L_7%(int32_t)((int32_t)60)));
		float L_8 = __this->get_time_13();
		int32_t L_9 = Mathf_FloorToInt_m4005035722(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)L_9/(int32_t)((int32_t)60)));
		int32_t L_10 = V_0;
		if (!L_10)
		{
			goto IL_006a;
		}
	}
	{
		String_t* L_11 = Int32_ToString_m2960866144((&V_0), /*hidden argument*/NULL);
		__this->set_minutestr_23(L_11);
	}

IL_006a:
	{
		int32_t L_12 = V_1;
		if (!L_12)
		{
			goto IL_007d;
		}
	}
	{
		String_t* L_13 = Int32_ToString_m2960866144((&V_1), /*hidden argument*/NULL);
		__this->set_secondstr_24(L_13);
	}

IL_007d:
	{
		int32_t L_14 = V_0;
		if ((((int32_t)L_14) >= ((int32_t)((int32_t)10))))
		{
			goto IL_009c;
		}
	}
	{
		String_t* L_15 = Int32_ToString_m2960866144((&V_0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral372029326, L_15, /*hidden argument*/NULL);
		__this->set_minutestr_23(L_16);
	}

IL_009c:
	{
		int32_t L_17 = V_1;
		if ((((int32_t)L_17) >= ((int32_t)((int32_t)10))))
		{
			goto IL_00bb;
		}
	}
	{
		String_t* L_18 = Int32_ToString_m2960866144((&V_1), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_19 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral372029326, L_18, /*hidden argument*/NULL);
		__this->set_secondstr_24(L_19);
	}

IL_00bb:
	{
		Text_t356221433 * L_20 = __this->get_uiText_29();
		String_t* L_21 = __this->get_minutestr_23();
		String_t* L_22 = __this->get_secondstr_24();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_23 = String_Concat_m612901809(NULL /*static, unused*/, L_21, _stringLiteral372029336, L_22, /*hidden argument*/NULL);
		NullCheck(L_20);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_20, L_23);
		return;
	}
}
// System.Void HighScore::but()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* HighScore_t2402950694_il2cpp_TypeInfo_var;
extern const uint32_t HighScore_but_m4158015102_MetadataUsageId;
extern "C"  void HighScore_but_m4158015102 (HighScore_t2402950694 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (HighScore_but_m4158015102_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_str_9(L_0);
		GameObject_t1756533147 * L_1 = __this->get_ch_12();
		NullCheck(L_1);
		String_t* L_2 = Object_get_name_m2079638459(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		CharU5BU5D_t1328083999* L_3 = String_ToCharArray_m870309954(L_2, /*hidden argument*/NULL);
		__this->set_getnum_8(L_3);
		CharU5BU5D_t1328083999* L_4 = __this->get_getnum_8();
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 6);
		String_t* L_5 = Char_ToString_m1976753030(((L_4)->GetAddressAt(static_cast<il2cpp_array_size_t>(6))), /*hidden argument*/NULL);
		__this->set_str_9(L_5);
		CharU5BU5D_t1328083999* L_6 = __this->get_getnum_8();
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 7);
		int32_t L_7 = 7;
		uint16_t L_8 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		if ((((int32_t)L_8) == ((int32_t)((int32_t)97))))
		{
			goto IL_0069;
		}
	}
	{
		String_t* L_9 = __this->get_str_9();
		CharU5BU5D_t1328083999* L_10 = __this->get_getnum_8();
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 7);
		String_t* L_11 = Char_ToString_m1976753030(((L_10)->GetAddressAt(static_cast<il2cpp_array_size_t>(7))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = String_Concat_m2596409543(NULL /*static, unused*/, L_9, L_11, /*hidden argument*/NULL);
		__this->set_str_9(L_12);
	}

IL_0069:
	{
		String_t* L_13 = __this->get_str_9();
		int32_t L_14 = Int32_Parse_m3683414232(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		__this->set_index_6(L_14);
		IL2CPP_RUNTIME_CLASS_INIT(HighScore_t2402950694_il2cpp_TypeInfo_var);
		BooleanU5BU5D_t3568034315* L_15 = ((HighScore_t2402950694_StaticFields*)HighScore_t2402950694_il2cpp_TypeInfo_var->static_fields)->get_flag_7();
		int32_t L_16 = __this->get_index_6();
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, L_16);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(L_16), (bool)1);
		__this->set_cnt_5(0);
		__this->set_i_2(1);
		goto IL_00ea;
	}

IL_009a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(HighScore_t2402950694_il2cpp_TypeInfo_var);
		BooleanU5BU5D_t3568034315* L_17 = ((HighScore_t2402950694_StaticFields*)HighScore_t2402950694_il2cpp_TypeInfo_var->static_fields)->get_flag_7();
		int32_t L_18 = __this->get_i_2();
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_18);
		int32_t L_19 = L_18;
		uint8_t L_20 = (L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		if (!L_20)
		{
			goto IL_00b9;
		}
	}
	{
		int32_t L_21 = __this->get_cnt_5();
		__this->set_cnt_5(((int32_t)((int32_t)L_21+(int32_t)1)));
	}

IL_00b9:
	{
		int32_t L_22 = __this->get_cnt_5();
		if ((((int32_t)L_22) < ((int32_t)((int32_t)93))))
		{
			goto IL_00dc;
		}
	}
	{
		Time_set_timeScale_m2194722837(NULL /*static, unused*/, (0.0f), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(HighScore_t2402950694_il2cpp_TypeInfo_var);
		((HighScore_t2402950694_StaticFields*)HighScore_t2402950694_il2cpp_TypeInfo_var->static_fields)->set_finish_11((bool)1);
		HighScore_savescore_m461077820(__this, /*hidden argument*/NULL);
	}

IL_00dc:
	{
		int32_t L_23 = __this->get_i_2();
		__this->set_i_2(((int32_t)((int32_t)L_23+(int32_t)1)));
	}

IL_00ea:
	{
		int32_t L_24 = __this->get_i_2();
		IL2CPP_RUNTIME_CLASS_INIT(HighScore_t2402950694_il2cpp_TypeInfo_var);
		BooleanU5BU5D_t3568034315* L_25 = ((HighScore_t2402950694_StaticFields*)HighScore_t2402950694_il2cpp_TypeInfo_var->static_fields)->get_flag_7();
		NullCheck(L_25);
		if ((((int32_t)L_24) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_25)->max_length)))))))
		{
			goto IL_009a;
		}
	}
	{
		return;
	}
}
// System.Void HighScore::savescore()
extern Il2CppClass* HighScore_t2402950694_il2cpp_TypeInfo_var;
extern Il2CppClass* GlobalCall_t2994192287_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3110053179;
extern Il2CppCodeGenString* _stringLiteral109622692;
extern const uint32_t HighScore_savescore_m461077820_MetadataUsageId;
extern "C"  void HighScore_savescore_m461077820 (HighScore_t2402950694 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (HighScore_savescore_m461077820_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(HighScore_t2402950694_il2cpp_TypeInfo_var);
		int32_t L_0 = ((HighScore_t2402950694_StaticFields*)HighScore_t2402950694_il2cpp_TypeInfo_var->static_fields)->get_score_14();
		IL2CPP_RUNTIME_CLASS_INIT(GlobalCall_t2994192287_il2cpp_TypeInfo_var);
		GlobalCall_setScore_m1324356053(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		int32_t L_1 = ((HighScore_t2402950694_StaticFields*)HighScore_t2402950694_il2cpp_TypeInfo_var->static_fields)->get_nh_30();
		if (L_1)
		{
			goto IL_0023;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(HighScore_t2402950694_il2cpp_TypeInfo_var);
		int32_t L_2 = ((HighScore_t2402950694_StaticFields*)HighScore_t2402950694_il2cpp_TypeInfo_var->static_fields)->get_score_14();
		((HighScore_t2402950694_StaticFields*)HighScore_t2402950694_il2cpp_TypeInfo_var->static_fields)->set_high_15(L_2);
		goto IL_0041;
	}

IL_0023:
	{
		bool L_3 = PlayerPrefs_HasKey_m1212656251(NULL /*static, unused*/, _stringLiteral3110053179, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0041;
		}
	}
	{
		int32_t L_4 = PlayerPrefs_GetInt_m2889062785(NULL /*static, unused*/, _stringLiteral3110053179, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(HighScore_t2402950694_il2cpp_TypeInfo_var);
		((HighScore_t2402950694_StaticFields*)HighScore_t2402950694_il2cpp_TypeInfo_var->static_fields)->set_high_15(L_4);
	}

IL_0041:
	{
		IL2CPP_RUNTIME_CLASS_INIT(HighScore_t2402950694_il2cpp_TypeInfo_var);
		bool L_5 = ((HighScore_t2402950694_StaticFields*)HighScore_t2402950694_il2cpp_TypeInfo_var->static_fields)->get_finish_11();
		if (!L_5)
		{
			goto IL_007d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(HighScore_t2402950694_il2cpp_TypeInfo_var);
		int32_t L_6 = ((HighScore_t2402950694_StaticFields*)HighScore_t2402950694_il2cpp_TypeInfo_var->static_fields)->get_score_14();
		int32_t L_7 = ((HighScore_t2402950694_StaticFields*)HighScore_t2402950694_il2cpp_TypeInfo_var->static_fields)->get_high_15();
		if ((((int32_t)L_6) > ((int32_t)L_7)))
		{
			goto IL_007d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(HighScore_t2402950694_il2cpp_TypeInfo_var);
		int32_t L_8 = ((HighScore_t2402950694_StaticFields*)HighScore_t2402950694_il2cpp_TypeInfo_var->static_fields)->get_score_14();
		PlayerPrefs_SetInt_m3351928596(NULL /*static, unused*/, _stringLiteral3110053179, L_8, /*hidden argument*/NULL);
		int32_t L_9 = PlayerPrefs_GetInt_m2889062785(NULL /*static, unused*/, _stringLiteral3110053179, /*hidden argument*/NULL);
		((HighScore_t2402950694_StaticFields*)HighScore_t2402950694_il2cpp_TypeInfo_var->static_fields)->set_high_15(L_9);
		PlayerPrefs_Save_m212854761(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_007d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(HighScore_t2402950694_il2cpp_TypeInfo_var);
		int32_t L_10 = ((HighScore_t2402950694_StaticFields*)HighScore_t2402950694_il2cpp_TypeInfo_var->static_fields)->get_high_15();
		IL2CPP_RUNTIME_CLASS_INIT(GlobalCall_t2994192287_il2cpp_TypeInfo_var);
		GlobalCall_setHigh_m735216011(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		int32_t L_11 = ((HighScore_t2402950694_StaticFields*)HighScore_t2402950694_il2cpp_TypeInfo_var->static_fields)->get_high_15();
		int32_t L_12 = L_11;
		Il2CppObject * L_13 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_12);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral109622692, L_13, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		HighScore_Complete_m3901898926(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HighScore::Complete()
extern Il2CppCodeGenString* _stringLiteral1624192333;
extern const uint32_t HighScore_Complete_m3901898926_MetadataUsageId;
extern "C"  void HighScore_Complete_m3901898926 (HighScore_t2402950694 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (HighScore_Complete_m3901898926_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		SceneManager_LoadScene_m1619949821(NULL /*static, unused*/, _stringLiteral1624192333, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LayerDepth0::.ctor()
extern "C"  void LayerDepth0__ctor_m3814776793 (LayerDepth0_t2725382046 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LayerDepth0::.cctor()
extern "C"  void LayerDepth0__cctor_m3681612936 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void LayerDepth0::OnGUI()
extern Il2CppClass* LayerDepth0_t2725382046_il2cpp_TypeInfo_var;
extern Il2CppClass* GUI_t4082743951_il2cpp_TypeInfo_var;
extern const uint32_t LayerDepth0_OnGUI_m3795493627_MetadataUsageId;
extern "C"  void LayerDepth0_OnGUI_m3795493627 (LayerDepth0_t2725382046 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LayerDepth0_OnGUI_m3795493627_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(LayerDepth0_t2725382046_il2cpp_TypeInfo_var);
		int32_t L_0 = ((LayerDepth0_t2725382046_StaticFields*)LayerDepth0_t2725382046_il2cpp_TypeInfo_var->static_fields)->get_guiDepth_2();
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_set_depth_m3824351935(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LayerDepth1::.ctor()
extern "C"  void LayerDepth1__ctor_m3811290904 (LayerDepth1_t2725382045 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LayerDepth1::.cctor()
extern Il2CppClass* LayerDepth1_t2725382045_il2cpp_TypeInfo_var;
extern const uint32_t LayerDepth1__cctor_m3566580839_MetadataUsageId;
extern "C"  void LayerDepth1__cctor_m3566580839 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LayerDepth1__cctor_m3566580839_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((LayerDepth1_t2725382045_StaticFields*)LayerDepth1_t2725382045_il2cpp_TypeInfo_var->static_fields)->set_guiDepth_2(1);
		return;
	}
}
// System.Void LayerDepth1::OnGUI()
extern Il2CppClass* LayerDepth1_t2725382045_il2cpp_TypeInfo_var;
extern Il2CppClass* GUI_t4082743951_il2cpp_TypeInfo_var;
extern const uint32_t LayerDepth1_OnGUI_m3792012028_MetadataUsageId;
extern "C"  void LayerDepth1_OnGUI_m3792012028 (LayerDepth1_t2725382045 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LayerDepth1_OnGUI_m3792012028_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(LayerDepth1_t2725382045_il2cpp_TypeInfo_var);
		int32_t L_0 = ((LayerDepth1_t2725382045_StaticFields*)LayerDepth1_t2725382045_il2cpp_TypeInfo_var->static_fields)->get_guiDepth_2();
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t4082743951_il2cpp_TypeInfo_var);
		GUI_set_depth_m3824351935(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void OptionController::.ctor()
extern const MethodInfo* GameObject_GetComponent_TisAudioSource_t1135106623_m3309832039_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1455837539;
extern const uint32_t OptionController__ctor_m3372235064_MetadataUsageId;
extern "C"  void OptionController__ctor_m3372235064 (OptionController_t3288053769 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OptionController__ctor_m3372235064_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral1455837539, /*hidden argument*/NULL);
		NullCheck(L_0);
		AudioSource_t1135106623 * L_1 = GameObject_GetComponent_TisAudioSource_t1135106623_m3309832039(L_0, /*hidden argument*/GameObject_GetComponent_TisAudioSource_t1135106623_m3309832039_MethodInfo_var);
		__this->set_audiosource_4(L_1);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void OptionController::Start()
extern Il2CppClass* UnityAction_1_t897193173_il2cpp_TypeInfo_var;
extern Il2CppClass* GlobalCall_t2994192287_il2cpp_TypeInfo_var;
extern const MethodInfo* OptionController_gameSoundStatusChanged_m2519406738_MethodInfo_var;
extern const MethodInfo* UnityAction_1__ctor_m1968084291_MethodInfo_var;
extern const MethodInfo* UnityEvent_1_AddListener_m1708363187_MethodInfo_var;
extern const uint32_t OptionController_Start_m884063112_MetadataUsageId;
extern "C"  void OptionController_Start_m884063112 (OptionController_t3288053769 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OptionController_Start_m884063112_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Toggle_t3976754468 * L_0 = __this->get_currenttoggle_3();
		NullCheck(L_0);
		ToggleEvent_t1896830814 * L_1 = L_0->get_onValueChanged_19();
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)OptionController_gameSoundStatusChanged_m2519406738_MethodInfo_var);
		UnityAction_1_t897193173 * L_3 = (UnityAction_1_t897193173 *)il2cpp_codegen_object_new(UnityAction_1_t897193173_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m1968084291(L_3, __this, L_2, /*hidden argument*/UnityAction_1__ctor_m1968084291_MethodInfo_var);
		NullCheck(L_1);
		UnityEvent_1_AddListener_m1708363187(L_1, L_3, /*hidden argument*/UnityEvent_1_AddListener_m1708363187_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(GlobalCall_t2994192287_il2cpp_TypeInfo_var);
		float L_4 = ((GlobalCall_t2994192287_StaticFields*)GlobalCall_t2994192287_il2cpp_TypeInfo_var->static_fields)->get_currentBGM_2();
		if ((!(((float)L_4) == ((float)(1.0f)))))
		{
			goto IL_003c;
		}
	}
	{
		Toggle_t3976754468 * L_5 = __this->get_currenttoggle_3();
		NullCheck(L_5);
		Toggle_set_isOn_m4022556286(L_5, (bool)1, /*hidden argument*/NULL);
		goto IL_0048;
	}

IL_003c:
	{
		Toggle_t3976754468 * L_6 = __this->get_currenttoggle_3();
		NullCheck(L_6);
		Toggle_set_isOn_m4022556286(L_6, (bool)0, /*hidden argument*/NULL);
	}

IL_0048:
	{
		return;
	}
}
// System.Void OptionController::OnBackButtonClicked()
extern Il2CppClass* GlobalCall_t2994192287_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral69878724;
extern Il2CppCodeGenString* _stringLiteral2435266816;
extern const uint32_t OptionController_OnBackButtonClicked_m312055907_MetadataUsageId;
extern "C"  void OptionController_OnBackButtonClicked_m312055907 (OptionController_t3288053769 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OptionController_OnBackButtonClicked_m312055907_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GlobalCall_t2994192287_il2cpp_TypeInfo_var);
		bool L_0 = ((GlobalCall_t2994192287_StaticFields*)GlobalCall_t2994192287_il2cpp_TypeInfo_var->static_fields)->get_isprevplay_3();
		if (!L_0)
		{
			goto IL_0019;
		}
	}
	{
		SceneManager_LoadScene_m1619949821(NULL /*static, unused*/, _stringLiteral69878724, /*hidden argument*/NULL);
		goto IL_002d;
	}

IL_0019:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GlobalCall_t2994192287_il2cpp_TypeInfo_var);
		bool L_1 = ((GlobalCall_t2994192287_StaticFields*)GlobalCall_t2994192287_il2cpp_TypeInfo_var->static_fields)->get_isprevplay_3();
		if (L_1)
		{
			goto IL_002d;
		}
	}
	{
		SceneManager_LoadScene_m1619949821(NULL /*static, unused*/, _stringLiteral2435266816, /*hidden argument*/NULL);
	}

IL_002d:
	{
		return;
	}
}
// System.Void OptionController::gameSoundStatusChanged(System.Boolean)
extern Il2CppClass* GlobalCall_t2994192287_il2cpp_TypeInfo_var;
extern const uint32_t OptionController_gameSoundStatusChanged_m2519406738_MetadataUsageId;
extern "C"  void OptionController_gameSoundStatusChanged_m2519406738 (OptionController_t3288053769 * __this, bool ___isclick0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OptionController_gameSoundStatusChanged_m2519406738_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = ___isclick0;
		if (L_0)
		{
			goto IL_0025;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GlobalCall_t2994192287_il2cpp_TypeInfo_var);
		((GlobalCall_t2994192287_StaticFields*)GlobalCall_t2994192287_il2cpp_TypeInfo_var->static_fields)->set_currentBGM_2((0.0f));
		AudioSource_t1135106623 * L_1 = __this->get_audiosource_4();
		float L_2 = ((GlobalCall_t2994192287_StaticFields*)GlobalCall_t2994192287_il2cpp_TypeInfo_var->static_fields)->get_currentBGM_2();
		NullCheck(L_1);
		AudioSource_set_volume_m2777308722(L_1, L_2, /*hidden argument*/NULL);
		goto IL_0045;
	}

IL_0025:
	{
		bool L_3 = ___isclick0;
		if (!L_3)
		{
			goto IL_0045;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GlobalCall_t2994192287_il2cpp_TypeInfo_var);
		((GlobalCall_t2994192287_StaticFields*)GlobalCall_t2994192287_il2cpp_TypeInfo_var->static_fields)->set_currentBGM_2((1.0f));
		AudioSource_t1135106623 * L_4 = __this->get_audiosource_4();
		float L_5 = ((GlobalCall_t2994192287_StaticFields*)GlobalCall_t2994192287_il2cpp_TypeInfo_var->static_fields)->get_currentBGM_2();
		NullCheck(L_4);
		AudioSource_set_volume_m2777308722(L_4, L_5, /*hidden argument*/NULL);
	}

IL_0045:
	{
		return;
	}
}
// System.Void OptionController::OnInitializationButtonClicked()
extern Il2CppCodeGenString* _stringLiteral1624159533;
extern Il2CppCodeGenString* _stringLiteral1291791064;
extern Il2CppCodeGenString* _stringLiteral4041293316;
extern const uint32_t OptionController_OnInitializationButtonClicked_m3843892390_MetadataUsageId;
extern "C"  void OptionController_OnInitializationButtonClicked_m3843892390 (OptionController_t3288053769 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OptionController_OnInitializationButtonClicked_m3843892390_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		PlayerPrefs_SetString_m2547809843(NULL /*static, unused*/, _stringLiteral1624159533, _stringLiteral1291791064, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m1399371129(__this, _stringLiteral4041293316, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator OptionController::MakeNotice()
extern Il2CppClass* U3CMakeNoticeU3Ec__Iterator1_t2390458154_il2cpp_TypeInfo_var;
extern const uint32_t OptionController_MakeNotice_m3253978076_MetadataUsageId;
extern "C"  Il2CppObject * OptionController_MakeNotice_m3253978076 (OptionController_t3288053769 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (OptionController_MakeNotice_m3253978076_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	U3CMakeNoticeU3Ec__Iterator1_t2390458154 * V_0 = NULL;
	{
		U3CMakeNoticeU3Ec__Iterator1_t2390458154 * L_0 = (U3CMakeNoticeU3Ec__Iterator1_t2390458154 *)il2cpp_codegen_object_new(U3CMakeNoticeU3Ec__Iterator1_t2390458154_il2cpp_TypeInfo_var);
		U3CMakeNoticeU3Ec__Iterator1__ctor_m3106708969(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CMakeNoticeU3Ec__Iterator1_t2390458154 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3Ef__this_2(__this);
		U3CMakeNoticeU3Ec__Iterator1_t2390458154 * L_2 = V_0;
		return L_2;
	}
}
// System.Void OptionController/<MakeNotice>c__Iterator1::.ctor()
extern "C"  void U3CMakeNoticeU3Ec__Iterator1__ctor_m3106708969 (U3CMakeNoticeU3Ec__Iterator1_t2390458154 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object OptionController/<MakeNotice>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CMakeNoticeU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3561824315 (U3CMakeNoticeU3Ec__Iterator1_t2390458154 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Object OptionController/<MakeNotice>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CMakeNoticeU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m3080120787 (U3CMakeNoticeU3Ec__Iterator1_t2390458154 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = __this->get_U24current_1();
		return L_0;
	}
}
// System.Boolean OptionController/<MakeNotice>c__Iterator1::MoveNext()
extern Il2CppClass* WaitForSeconds_t3839502067_il2cpp_TypeInfo_var;
extern const uint32_t U3CMakeNoticeU3Ec__Iterator1_MoveNext_m1396499911_MetadataUsageId;
extern "C"  bool U3CMakeNoticeU3Ec__Iterator1_MoveNext_m1396499911 (U3CMakeNoticeU3Ec__Iterator1_t2390458154 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CMakeNoticeU3Ec__Iterator1_MoveNext_m1396499911_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = __this->get_U24PC_0();
		V_0 = L_0;
		__this->set_U24PC_0((-1));
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_004e;
		}
	}
	{
		goto IL_0066;
	}

IL_0021:
	{
		OptionController_t3288053769 * L_2 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_2);
		GameObject_t1756533147 * L_3 = L_2->get_notice_2();
		NullCheck(L_3);
		GameObject_SetActive_m2887581199(L_3, (bool)1, /*hidden argument*/NULL);
		WaitForSeconds_t3839502067 * L_4 = (WaitForSeconds_t3839502067 *)il2cpp_codegen_object_new(WaitForSeconds_t3839502067_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m1990515539(L_4, (1.5f), /*hidden argument*/NULL);
		__this->set_U24current_1(L_4);
		__this->set_U24PC_0(1);
		goto IL_0068;
	}

IL_004e:
	{
		OptionController_t3288053769 * L_5 = __this->get_U3CU3Ef__this_2();
		NullCheck(L_5);
		GameObject_t1756533147 * L_6 = L_5->get_notice_2();
		NullCheck(L_6);
		GameObject_SetActive_m2887581199(L_6, (bool)0, /*hidden argument*/NULL);
		__this->set_U24PC_0((-1));
	}

IL_0066:
	{
		return (bool)0;
	}

IL_0068:
	{
		return (bool)1;
	}
	// Dead block : IL_006a: ldloc.1
}
// System.Void OptionController/<MakeNotice>c__Iterator1::Dispose()
extern "C"  void U3CMakeNoticeU3Ec__Iterator1_Dispose_m264160716 (U3CMakeNoticeU3Ec__Iterator1_t2390458154 * __this, const MethodInfo* method)
{
	{
		__this->set_U24PC_0((-1));
		return;
	}
}
// System.Void OptionController/<MakeNotice>c__Iterator1::Reset()
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CMakeNoticeU3Ec__Iterator1_Reset_m1088937550_MetadataUsageId;
extern "C"  void U3CMakeNoticeU3Ec__Iterator1_Reset_m1088937550 (U3CMakeNoticeU3Ec__Iterator1_t2390458154 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U3CMakeNoticeU3Ec__Iterator1_Reset_m1088937550_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void PlayController::.ctor()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t PlayController__ctor_m2087269799_MetadataUsageId;
extern "C"  void PlayController__ctor_m2087269799 (PlayController_t2024502094 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayController__ctor_m2087269799_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Vector2_t2243707579  L_0 = Vector2_get_zero_m3966848876(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_adjustHotSpot_4(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_str_23(L_1);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayController::Start()
extern Il2CppClass* CharU5BU5D_t1328083999_il2cpp_TypeInfo_var;
extern Il2CppClass* BooleanU5BU5D_t3568034315_il2cpp_TypeInfo_var;
extern Il2CppClass* PlayController_t2024502094_il2cpp_TypeInfo_var;
extern const uint32_t PlayController_Start_m1038701147_MetadataUsageId;
extern "C"  void PlayController_Start_m1038701147 (PlayController_t2024502094 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayController_Start_m1038701147_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		AudioSource_t1135106623 * L_0 = __this->get_audiosource_24();
		NullCheck(L_0);
		AudioSource_set_volume_m2777308722(L_0, (1.0f), /*hidden argument*/NULL);
		__this->set_getnum_20(((CharU5BU5D_t1328083999*)SZArrayNew(CharU5BU5D_t1328083999_il2cpp_TypeInfo_var, (uint32_t)((int32_t)9))));
		((PlayController_t2024502094_StaticFields*)PlayController_t2024502094_il2cpp_TypeInfo_var->static_fields)->set_flag_19(((BooleanU5BU5D_t3568034315*)SZArrayNew(BooleanU5BU5D_t3568034315_il2cpp_TypeInfo_var, (uint32_t)((int32_t)100))));
		((PlayController_t2024502094_StaticFields*)PlayController_t2024502094_il2cpp_TypeInfo_var->static_fields)->set_time_21((0.0f));
		return;
	}
}
// System.Void PlayController::OnVolumeButtonClicked()
extern "C"  void PlayController_OnVolumeButtonClicked_m898487635 (PlayController_t2024502094 * __this, const MethodInfo* method)
{
	{
		AudioSource_t1135106623 * L_0 = __this->get_audiosource_24();
		NullCheck(L_0);
		float L_1 = AudioSource_get_volume_m66289169(L_0, /*hidden argument*/NULL);
		if ((!(((float)L_1) == ((float)(1.0f)))))
		{
			goto IL_0042;
		}
	}
	{
		GameObject_t1756533147 * L_2 = __this->get_on_25();
		NullCheck(L_2);
		GameObject_SetActive_m2887581199(L_2, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_3 = __this->get_off_26();
		NullCheck(L_3);
		GameObject_SetActive_m2887581199(L_3, (bool)1, /*hidden argument*/NULL);
		AudioSource_t1135106623 * L_4 = __this->get_audiosource_24();
		NullCheck(L_4);
		AudioSource_set_volume_m2777308722(L_4, (0.0f), /*hidden argument*/NULL);
		goto IL_007f;
	}

IL_0042:
	{
		AudioSource_t1135106623 * L_5 = __this->get_audiosource_24();
		NullCheck(L_5);
		float L_6 = AudioSource_get_volume_m66289169(L_5, /*hidden argument*/NULL);
		if ((!(((float)L_6) == ((float)(0.0f)))))
		{
			goto IL_007f;
		}
	}
	{
		GameObject_t1756533147 * L_7 = __this->get_on_25();
		NullCheck(L_7);
		GameObject_SetActive_m2887581199(L_7, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_8 = __this->get_off_26();
		NullCheck(L_8);
		GameObject_SetActive_m2887581199(L_8, (bool)1, /*hidden argument*/NULL);
		AudioSource_t1135106623 * L_9 = __this->get_audiosource_24();
		NullCheck(L_9);
		AudioSource_set_volume_m2777308722(L_9, (1.0f), /*hidden argument*/NULL);
	}

IL_007f:
	{
		return;
	}
}
// System.Void PlayController::but()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* PlayController_t2024502094_il2cpp_TypeInfo_var;
extern const uint32_t PlayController_but_m3194425450_MetadataUsageId;
extern "C"  void PlayController_but_m3194425450 (PlayController_t2024502094 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayController_but_m3194425450_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_str_23(L_0);
		GameObject_t1756533147 * L_1 = __this->get_ch_8();
		NullCheck(L_1);
		String_t* L_2 = Object_get_name_m2079638459(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		CharU5BU5D_t1328083999* L_3 = String_ToCharArray_m870309954(L_2, /*hidden argument*/NULL);
		__this->set_getnum_20(L_3);
		CharU5BU5D_t1328083999* L_4 = __this->get_getnum_20();
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 6);
		String_t* L_5 = Char_ToString_m1976753030(((L_4)->GetAddressAt(static_cast<il2cpp_array_size_t>(6))), /*hidden argument*/NULL);
		__this->set_str_23(L_5);
		CharU5BU5D_t1328083999* L_6 = __this->get_getnum_20();
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 7);
		int32_t L_7 = 7;
		uint16_t L_8 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_7));
		if ((((int32_t)L_8) == ((int32_t)((int32_t)97))))
		{
			goto IL_0069;
		}
	}
	{
		String_t* L_9 = __this->get_str_23();
		CharU5BU5D_t1328083999* L_10 = __this->get_getnum_20();
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 7);
		String_t* L_11 = Char_ToString_m1976753030(((L_10)->GetAddressAt(static_cast<il2cpp_array_size_t>(7))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = String_Concat_m2596409543(NULL /*static, unused*/, L_9, L_11, /*hidden argument*/NULL);
		__this->set_str_23(L_12);
	}

IL_0069:
	{
		String_t* L_13 = __this->get_str_23();
		int32_t L_14 = Int32_Parse_m3683414232(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		__this->set_index_18(L_14);
		BooleanU5BU5D_t3568034315* L_15 = ((PlayController_t2024502094_StaticFields*)PlayController_t2024502094_il2cpp_TypeInfo_var->static_fields)->get_flag_19();
		int32_t L_16 = __this->get_index_18();
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, L_16);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(L_16), (bool)1);
		__this->set_cnt_17(0);
		__this->set_i_14(1);
		goto IL_00e5;
	}

IL_009a:
	{
		BooleanU5BU5D_t3568034315* L_17 = ((PlayController_t2024502094_StaticFields*)PlayController_t2024502094_il2cpp_TypeInfo_var->static_fields)->get_flag_19();
		int32_t L_18 = __this->get_i_14();
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_18);
		int32_t L_19 = L_18;
		uint8_t L_20 = (L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		if (!L_20)
		{
			goto IL_00b9;
		}
	}
	{
		int32_t L_21 = __this->get_cnt_17();
		__this->set_cnt_17(((int32_t)((int32_t)L_21+(int32_t)1)));
	}

IL_00b9:
	{
		int32_t L_22 = __this->get_cnt_17();
		if ((((int32_t)L_22) < ((int32_t)((int32_t)93))))
		{
			goto IL_00d7;
		}
	}
	{
		Time_set_timeScale_m2194722837(NULL /*static, unused*/, (0.0f), /*hidden argument*/NULL);
		__this->set_finish_13((bool)1);
	}

IL_00d7:
	{
		int32_t L_23 = __this->get_i_14();
		__this->set_i_14(((int32_t)((int32_t)L_23+(int32_t)1)));
	}

IL_00e5:
	{
		int32_t L_24 = __this->get_i_14();
		BooleanU5BU5D_t3568034315* L_25 = ((PlayController_t2024502094_StaticFields*)PlayController_t2024502094_il2cpp_TypeInfo_var->static_fields)->get_flag_19();
		NullCheck(L_25);
		if ((((int32_t)L_24) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_25)->max_length)))))))
		{
			goto IL_009a;
		}
	}
	{
		return;
	}
}
// System.Void PlayController::SetTrueVisibleButton()
extern "C"  void PlayController_SetTrueVisibleButton_m978986607 (PlayController_t2024502094 * __this, const MethodInfo* method)
{
	{
		Button_t2872111280 * L_0 = __this->get_title_6();
		NullCheck(L_0);
		GameObject_t1756533147 * L_1 = Component_get_gameObject_m3105766835(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		GameObject_SetActive_m2887581199(L_1, (bool)1, /*hidden argument*/NULL);
		Button_t2872111280 * L_2 = __this->get_back_7();
		NullCheck(L_2);
		GameObject_t1756533147 * L_3 = Component_get_gameObject_m3105766835(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		GameObject_SetActive_m2887581199(L_3, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayController::OnTitleButtonClicked()
extern Il2CppCodeGenString* _stringLiteral2435266816;
extern const uint32_t PlayController_OnTitleButtonClicked_m4000853203_MetadataUsageId;
extern "C"  void PlayController_OnTitleButtonClicked_m4000853203 (PlayController_t2024502094 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayController_OnTitleButtonClicked_m4000853203_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Time_set_timeScale_m2194722837(NULL /*static, unused*/, (1.0f), /*hidden argument*/NULL);
		SceneManager_LoadScene_m1619949821(NULL /*static, unused*/, _stringLiteral2435266816, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayController::OnOptionButtonClicked()
extern Il2CppClass* GlobalCall_t2994192287_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3910992875;
extern const uint32_t PlayController_OnOptionButtonClicked_m1707671980_MetadataUsageId;
extern "C"  void PlayController_OnOptionButtonClicked_m1707671980 (PlayController_t2024502094 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PlayController_OnOptionButtonClicked_m1707671980_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GlobalCall_t2994192287_il2cpp_TypeInfo_var);
		((GlobalCall_t2994192287_StaticFields*)GlobalCall_t2994192287_il2cpp_TypeInfo_var->static_fields)->set_isprevplay_3((bool)1);
		SceneManager_LoadScene_m1619949821(NULL /*static, unused*/, _stringLiteral3910992875, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayController::OnBackButtonClicked()
extern "C"  void PlayController_OnBackButtonClicked_m1260219122 (PlayController_t2024502094 * __this, const MethodInfo* method)
{
	float G_B3_0 = 0.0f;
	{
		GameObject_t1756533147 * L_0 = __this->get_potato_inside_10();
		NullCheck(L_0);
		GameObject_SetActive_m2887581199(L_0, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_1 = __this->get_potato_outside_11();
		NullCheck(L_1);
		GameObject_SetActive_m2887581199(L_1, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_2 = __this->get_menu_9();
		NullCheck(L_2);
		GameObject_SetActive_m2887581199(L_2, (bool)0, /*hidden argument*/NULL);
		Button_t2872111280 * L_3 = __this->get_title_6();
		NullCheck(L_3);
		GameObject_t1756533147 * L_4 = Component_get_gameObject_m3105766835(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		GameObject_SetActive_m2887581199(L_4, (bool)0, /*hidden argument*/NULL);
		Button_t2872111280 * L_5 = __this->get_back_7();
		NullCheck(L_5);
		GameObject_t1756533147 * L_6 = Component_get_gameObject_m3105766835(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		GameObject_SetActive_m2887581199(L_6, (bool)0, /*hidden argument*/NULL);
		float L_7 = Time_get_timeScale_m3151482970(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((float)L_7) > ((float)(0.0f)))))
		{
			goto IL_005f;
		}
	}
	{
		G_B3_0 = (0.0f);
		goto IL_0064;
	}

IL_005f:
	{
		G_B3_0 = (1.0f);
	}

IL_0064:
	{
		Time_set_timeScale_m2194722837(NULL /*static, unused*/, G_B3_0, /*hidden argument*/NULL);
		__this->set_pause_12((bool)1);
		return;
	}
}
// System.Void PlayController::OnPauseButtonClicked()
extern "C"  void PlayController_OnPauseButtonClicked_m4120296361 (PlayController_t2024502094 * __this, const MethodInfo* method)
{
	float G_B3_0 = 0.0f;
	{
		GameObject_t1756533147 * L_0 = __this->get_potato_inside_10();
		NullCheck(L_0);
		GameObject_SetActive_m2887581199(L_0, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_1 = __this->get_potato_outside_11();
		NullCheck(L_1);
		GameObject_SetActive_m2887581199(L_1, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_2 = __this->get_menu_9();
		NullCheck(L_2);
		GameObject_SetActive_m2887581199(L_2, (bool)1, /*hidden argument*/NULL);
		PlayController_SetTrueVisibleButton_m978986607(__this, /*hidden argument*/NULL);
		float L_3 = Time_get_timeScale_m3151482970(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((float)L_3) > ((float)(0.0f)))))
		{
			goto IL_0043;
		}
	}
	{
		G_B3_0 = (0.0f);
		goto IL_0048;
	}

IL_0043:
	{
		G_B3_0 = (1.0f);
	}

IL_0048:
	{
		Time_set_timeScale_m2194722837(NULL /*static, unused*/, G_B3_0, /*hidden argument*/NULL);
		__this->set_pause_12((bool)0);
		return;
	}
}
// System.Void ResultController::.ctor()
extern "C"  void ResultController__ctor_m2479552544 (ResultController_t1522536863 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ResultController::Start()
extern Il2CppClass* GlobalCall_t2994192287_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisText_t356221433_m1217399699_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1755609506;
extern Il2CppCodeGenString* _stringLiteral372029326;
extern Il2CppCodeGenString* _stringLiteral372029336;
extern Il2CppCodeGenString* _stringLiteral3457520297;
extern const uint32_t ResultController_Start_m1286081004_MetadataUsageId;
extern "C"  void ResultController_Start_m1286081004 (ResultController_t1522536863 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ResultController_Start_m1286081004_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(GlobalCall_t2994192287_il2cpp_TypeInfo_var);
		int32_t L_0 = GlobalCall_getScore_m1953950224(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_score_2(L_0);
		int32_t L_1 = __this->get_score_2();
		int32_t L_2 = L_1;
		Il2CppObject * L_3 = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &L_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m56707527(NULL /*static, unused*/, _stringLiteral1755609506, L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		int32_t L_5 = GlobalCall_getOrigin_m2405469574(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_6 = GlobalCall_getHigh_m3144168644(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_5) == ((int32_t)L_6)))
		{
			goto IL_0051;
		}
	}
	{
		GameObject_t1756533147 * L_7 = __this->get_normal_8();
		NullCheck(L_7);
		GameObject_SetActive_m2887581199(L_7, (bool)0, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_8 = __this->get_best_9();
		NullCheck(L_8);
		GameObject_SetActive_m2887581199(L_8, (bool)1, /*hidden argument*/NULL);
		goto IL_0069;
	}

IL_0051:
	{
		GameObject_t1756533147 * L_9 = __this->get_normal_8();
		NullCheck(L_9);
		GameObject_SetActive_m2887581199(L_9, (bool)1, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_10 = __this->get_best_9();
		NullCheck(L_10);
		GameObject_SetActive_m2887581199(L_10, (bool)0, /*hidden argument*/NULL);
	}

IL_0069:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GlobalCall_t2994192287_il2cpp_TypeInfo_var);
		int32_t L_11 = GlobalCall_getScore_m1953950224(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_score_2(L_11);
		int32_t L_12 = __this->get_score_2();
		__this->set_m_5(((int32_t)((int32_t)L_12/(int32_t)((int32_t)60))));
		int32_t L_13 = __this->get_score_2();
		__this->set_s_6(((int32_t)((int32_t)L_13%(int32_t)((int32_t)60))));
		int32_t L_14 = __this->get_m_5();
		if (!L_14)
		{
			goto IL_00ae;
		}
	}
	{
		int32_t* L_15 = __this->get_address_of_m_5();
		String_t* L_16 = Int32_ToString_m2960866144(L_15, /*hidden argument*/NULL);
		__this->set_mstr_3(L_16);
	}

IL_00ae:
	{
		int32_t L_17 = __this->get_s_6();
		if (!L_17)
		{
			goto IL_00ca;
		}
	}
	{
		int32_t* L_18 = __this->get_address_of_s_6();
		String_t* L_19 = Int32_ToString_m2960866144(L_18, /*hidden argument*/NULL);
		__this->set_sstr_4(L_19);
	}

IL_00ca:
	{
		int32_t L_20 = __this->get_m_5();
		if ((((int32_t)L_20) >= ((int32_t)((int32_t)10))))
		{
			goto IL_00f2;
		}
	}
	{
		int32_t* L_21 = __this->get_address_of_m_5();
		String_t* L_22 = Int32_ToString_m2960866144(L_21, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_23 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral372029326, L_22, /*hidden argument*/NULL);
		__this->set_mstr_3(L_23);
	}

IL_00f2:
	{
		int32_t L_24 = __this->get_s_6();
		if ((((int32_t)L_24) >= ((int32_t)((int32_t)10))))
		{
			goto IL_011a;
		}
	}
	{
		int32_t* L_25 = __this->get_address_of_s_6();
		String_t* L_26 = Int32_ToString_m2960866144(L_25, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_27 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral372029326, L_26, /*hidden argument*/NULL);
		__this->set_sstr_4(L_27);
	}

IL_011a:
	{
		String_t* L_28 = __this->get_mstr_3();
		String_t* L_29 = __this->get_sstr_4();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_30 = String_Concat_m612901809(NULL /*static, unused*/, L_28, _stringLiteral372029336, L_29, /*hidden argument*/NULL);
		V_0 = L_30;
		GameObject_t1756533147 * L_31 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral3457520297, /*hidden argument*/NULL);
		NullCheck(L_31);
		Text_t356221433 * L_32 = GameObject_GetComponent_TisText_t356221433_m1217399699(L_31, /*hidden argument*/GameObject_GetComponent_TisText_t356221433_m1217399699_MethodInfo_var);
		String_t* L_33 = V_0;
		NullCheck(L_32);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_32, L_33);
		return;
	}
}
// System.Void ResultController::GoToMain()
extern Il2CppCodeGenString* _stringLiteral2435266816;
extern const uint32_t ResultController_GoToMain_m3962121524_MetadataUsageId;
extern "C"  void ResultController_GoToMain_m3962121524 (ResultController_t1522536863 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ResultController_GoToMain_m3962121524_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		SceneManager_LoadScene_m1619949821(NULL /*static, unused*/, _stringLiteral2435266816, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ResultController::GoToAgain()
extern Il2CppCodeGenString* _stringLiteral69878724;
extern const uint32_t ResultController_GoToAgain_m2547900681_MetadataUsageId;
extern "C"  void ResultController_GoToAgain_m2547900681 (ResultController_t1522536863 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ResultController_GoToAgain_m2547900681_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		SceneManager_LoadScene_m1619949821(NULL /*static, unused*/, _stringLiteral69878724, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TitleController::.ctor()
extern "C"  void TitleController__ctor_m1390827847 (TitleController_t391985954 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TitleController::Start()
extern Il2CppClass* GlobalCall_t2994192287_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisText_t356221433_m1217399699_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029326;
extern Il2CppCodeGenString* _stringLiteral372029336;
extern Il2CppCodeGenString* _stringLiteral1986007643;
extern const uint32_t TitleController_Start_m127862867_MetadataUsageId;
extern "C"  void TitleController_Start_m127862867 (TitleController_t391985954 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TitleController_Start_m127862867_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(GlobalCall_t2994192287_il2cpp_TypeInfo_var);
		int32_t L_0 = GlobalCall_getHigh_m3144168644(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_result_5(L_0);
		int32_t L_1 = __this->get_result_5();
		__this->set_m_8(((int32_t)((int32_t)L_1/(int32_t)((int32_t)60))));
		int32_t L_2 = __this->get_result_5();
		__this->set_s_9(((int32_t)((int32_t)L_2%(int32_t)((int32_t)60))));
		int32_t L_3 = __this->get_m_8();
		if (!L_3)
		{
			goto IL_0045;
		}
	}
	{
		int32_t* L_4 = __this->get_address_of_m_8();
		String_t* L_5 = Int32_ToString_m2960866144(L_4, /*hidden argument*/NULL);
		__this->set_mstr_6(L_5);
	}

IL_0045:
	{
		int32_t L_6 = __this->get_s_9();
		if (!L_6)
		{
			goto IL_0061;
		}
	}
	{
		int32_t* L_7 = __this->get_address_of_s_9();
		String_t* L_8 = Int32_ToString_m2960866144(L_7, /*hidden argument*/NULL);
		__this->set_sstr_7(L_8);
	}

IL_0061:
	{
		int32_t L_9 = __this->get_m_8();
		if ((((int32_t)L_9) >= ((int32_t)((int32_t)10))))
		{
			goto IL_0089;
		}
	}
	{
		int32_t* L_10 = __this->get_address_of_m_8();
		String_t* L_11 = Int32_ToString_m2960866144(L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral372029326, L_11, /*hidden argument*/NULL);
		__this->set_mstr_6(L_12);
	}

IL_0089:
	{
		int32_t L_13 = __this->get_s_9();
		if ((((int32_t)L_13) >= ((int32_t)((int32_t)10))))
		{
			goto IL_00b1;
		}
	}
	{
		int32_t* L_14 = __this->get_address_of_s_9();
		String_t* L_15 = Int32_ToString_m2960866144(L_14, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m2596409543(NULL /*static, unused*/, _stringLiteral372029326, L_15, /*hidden argument*/NULL);
		__this->set_sstr_7(L_16);
	}

IL_00b1:
	{
		String_t* L_17 = __this->get_mstr_6();
		String_t* L_18 = __this->get_sstr_7();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_19 = String_Concat_m612901809(NULL /*static, unused*/, L_17, _stringLiteral372029336, L_18, /*hidden argument*/NULL);
		V_0 = L_19;
		GameObject_t1756533147 * L_20 = GameObject_Find_m836511350(NULL /*static, unused*/, _stringLiteral1986007643, /*hidden argument*/NULL);
		NullCheck(L_20);
		Text_t356221433 * L_21 = GameObject_GetComponent_TisText_t356221433_m1217399699(L_20, /*hidden argument*/GameObject_GetComponent_TisText_t356221433_m1217399699_MethodInfo_var);
		String_t* L_22 = V_0;
		NullCheck(L_21);
		VirtActionInvoker1< String_t* >::Invoke(74 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_21, L_22);
		return;
	}
}
// System.Void TitleController::Update()
extern Il2CppClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern const uint32_t TitleController_Update_m2789011500_MetadataUsageId;
extern "C"  void TitleController_Update_m2789011500 (TitleController_t391985954 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TitleController_Update_m2789011500_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = Application_get_platform_m3989224144(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_0) == ((uint32_t)((int32_t)11)))))
		{
			goto IL_001d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_1 = Input_GetKey_m3849524999(NULL /*static, unused*/, ((int32_t)27), /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001d;
		}
	}
	{
		Application_Quit_m3885595876(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_001d:
	{
		return;
	}
}
// System.Void TitleController::OnStartButtonClicked()
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral69878724;
extern const uint32_t TitleController_OnStartButtonClicked_m2541716943_MetadataUsageId;
extern "C"  void TitleController_OnStartButtonClicked_m2541716943 (TitleController_t391985954 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TitleController_OnStartButtonClicked_m2541716943_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		SceneManager_LoadScene_m1619949821(NULL /*static, unused*/, _stringLiteral69878724, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TitleController::OnOptionButtonClicked()
extern Il2CppClass* GlobalCall_t2994192287_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3910992875;
extern const uint32_t TitleController_OnOptionButtonClicked_m2709104756_MetadataUsageId;
extern "C"  void TitleController_OnOptionButtonClicked_m2709104756 (TitleController_t391985954 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TitleController_OnOptionButtonClicked_m2709104756_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GlobalCall_t2994192287_il2cpp_TypeInfo_var);
		((GlobalCall_t2994192287_StaticFields*)GlobalCall_t2994192287_il2cpp_TypeInfo_var->static_fields)->set_isprevplay_3((bool)0);
		SceneManager_LoadScene_m1619949821(NULL /*static, unused*/, _stringLiteral3910992875, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
