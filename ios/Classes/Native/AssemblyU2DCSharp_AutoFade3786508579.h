﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// AutoFade
struct AutoFade_t3786508579;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.Mesh
struct Mesh_t1356156583;
// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AutoFade
struct  AutoFade_t3786508579  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Material AutoFade::mat
	Material_t193706927 * ___mat_3;
	// UnityEngine.Mesh AutoFade::mesh
	Mesh_t1356156583 * ___mesh_4;
	// System.String AutoFade::m_LevelName
	String_t* ___m_LevelName_5;
	// System.Int32 AutoFade::m_LevelIndex
	int32_t ___m_LevelIndex_6;
	// System.Boolean AutoFade::m_Fading
	bool ___m_Fading_7;

public:
	inline static int32_t get_offset_of_mat_3() { return static_cast<int32_t>(offsetof(AutoFade_t3786508579, ___mat_3)); }
	inline Material_t193706927 * get_mat_3() const { return ___mat_3; }
	inline Material_t193706927 ** get_address_of_mat_3() { return &___mat_3; }
	inline void set_mat_3(Material_t193706927 * value)
	{
		___mat_3 = value;
		Il2CppCodeGenWriteBarrier(&___mat_3, value);
	}

	inline static int32_t get_offset_of_mesh_4() { return static_cast<int32_t>(offsetof(AutoFade_t3786508579, ___mesh_4)); }
	inline Mesh_t1356156583 * get_mesh_4() const { return ___mesh_4; }
	inline Mesh_t1356156583 ** get_address_of_mesh_4() { return &___mesh_4; }
	inline void set_mesh_4(Mesh_t1356156583 * value)
	{
		___mesh_4 = value;
		Il2CppCodeGenWriteBarrier(&___mesh_4, value);
	}

	inline static int32_t get_offset_of_m_LevelName_5() { return static_cast<int32_t>(offsetof(AutoFade_t3786508579, ___m_LevelName_5)); }
	inline String_t* get_m_LevelName_5() const { return ___m_LevelName_5; }
	inline String_t** get_address_of_m_LevelName_5() { return &___m_LevelName_5; }
	inline void set_m_LevelName_5(String_t* value)
	{
		___m_LevelName_5 = value;
		Il2CppCodeGenWriteBarrier(&___m_LevelName_5, value);
	}

	inline static int32_t get_offset_of_m_LevelIndex_6() { return static_cast<int32_t>(offsetof(AutoFade_t3786508579, ___m_LevelIndex_6)); }
	inline int32_t get_m_LevelIndex_6() const { return ___m_LevelIndex_6; }
	inline int32_t* get_address_of_m_LevelIndex_6() { return &___m_LevelIndex_6; }
	inline void set_m_LevelIndex_6(int32_t value)
	{
		___m_LevelIndex_6 = value;
	}

	inline static int32_t get_offset_of_m_Fading_7() { return static_cast<int32_t>(offsetof(AutoFade_t3786508579, ___m_Fading_7)); }
	inline bool get_m_Fading_7() const { return ___m_Fading_7; }
	inline bool* get_address_of_m_Fading_7() { return &___m_Fading_7; }
	inline void set_m_Fading_7(bool value)
	{
		___m_Fading_7 = value;
	}
};

struct AutoFade_t3786508579_StaticFields
{
public:
	// AutoFade AutoFade::m_Instance
	AutoFade_t3786508579 * ___m_Instance_2;

public:
	inline static int32_t get_offset_of_m_Instance_2() { return static_cast<int32_t>(offsetof(AutoFade_t3786508579_StaticFields, ___m_Instance_2)); }
	inline AutoFade_t3786508579 * get_m_Instance_2() const { return ___m_Instance_2; }
	inline AutoFade_t3786508579 ** get_address_of_m_Instance_2() { return &___m_Instance_2; }
	inline void set_m_Instance_2(AutoFade_t3786508579 * value)
	{
		___m_Instance_2 = value;
		Il2CppCodeGenWriteBarrier(&___m_Instance_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
