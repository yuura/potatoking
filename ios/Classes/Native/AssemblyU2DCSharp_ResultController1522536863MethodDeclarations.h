﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ResultController
struct ResultController_t1522536863;

#include "codegen/il2cpp-codegen.h"

// System.Void ResultController::.ctor()
extern "C"  void ResultController__ctor_m2479552544 (ResultController_t1522536863 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResultController::Start()
extern "C"  void ResultController_Start_m1286081004 (ResultController_t1522536863 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResultController::GoToMain()
extern "C"  void ResultController_GoToMain_m3962121524 (ResultController_t1522536863 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ResultController::GoToAgain()
extern "C"  void ResultController_GoToAgain_m2547900681 (ResultController_t1522536863 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
