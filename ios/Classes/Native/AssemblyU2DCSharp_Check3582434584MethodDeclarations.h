﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Check
struct Check_t3582434584;

#include "codegen/il2cpp-codegen.h"

// System.Void Check::.ctor()
extern "C"  void Check__ctor_m2578362865 (Check_t3582434584 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Check::Start()
extern "C"  void Check_Start_m1734623497 (Check_t3582434584 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Check::Update()
extern "C"  void Check_Update_m392427046 (Check_t3582434584 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Check::onClick()
extern "C"  void Check_onClick_m1465768360 (Check_t3582434584 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
