﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Boolean[]
struct BooleanU5BU5D_t3568034315;
// System.Char[]
struct CharU5BU5D_t1328083999;
// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.Text
struct Text_t356221433;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HighScore
struct  HighScore_t2402950694  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 HighScore::i
	int32_t ___i_2;
	// System.Int32 HighScore::j
	int32_t ___j_3;
	// System.Int32 HighScore::num
	int32_t ___num_4;
	// System.Int32 HighScore::cnt
	int32_t ___cnt_5;
	// System.Int32 HighScore::index
	int32_t ___index_6;
	// System.Char[] HighScore::getnum
	CharU5BU5D_t1328083999* ___getnum_8;
	// System.String HighScore::str
	String_t* ___str_9;
	// System.Boolean HighScore::pause
	bool ___pause_10;
	// UnityEngine.GameObject HighScore::ch
	GameObject_t1756533147 * ___ch_12;
	// System.Single HighScore::time
	float ___time_13;
	// System.Int32 HighScore::m
	int32_t ___m_17;
	// System.Int32 HighScore::s
	int32_t ___s_18;
	// System.String HighScore::mstr
	String_t* ___mstr_19;
	// System.String HighScore::sstr
	String_t* ___sstr_20;
	// System.String HighScore::highstr
	String_t* ___highstr_21;
	// System.Single HighScore::current
	float ___current_22;
	// System.String HighScore::minutestr
	String_t* ___minutestr_23;
	// System.String HighScore::secondstr
	String_t* ___secondstr_24;
	// System.String HighScore::highsc
	String_t* ___highsc_25;
	// System.Int32 HighScore::sixty
	int32_t ___sixty_26;
	// System.Single HighScore::fsixty
	float ___fsixty_27;
	// System.Int32 HighScore::minute
	int32_t ___minute_28;
	// UnityEngine.UI.Text HighScore::uiText
	Text_t356221433 * ___uiText_29;

public:
	inline static int32_t get_offset_of_i_2() { return static_cast<int32_t>(offsetof(HighScore_t2402950694, ___i_2)); }
	inline int32_t get_i_2() const { return ___i_2; }
	inline int32_t* get_address_of_i_2() { return &___i_2; }
	inline void set_i_2(int32_t value)
	{
		___i_2 = value;
	}

	inline static int32_t get_offset_of_j_3() { return static_cast<int32_t>(offsetof(HighScore_t2402950694, ___j_3)); }
	inline int32_t get_j_3() const { return ___j_3; }
	inline int32_t* get_address_of_j_3() { return &___j_3; }
	inline void set_j_3(int32_t value)
	{
		___j_3 = value;
	}

	inline static int32_t get_offset_of_num_4() { return static_cast<int32_t>(offsetof(HighScore_t2402950694, ___num_4)); }
	inline int32_t get_num_4() const { return ___num_4; }
	inline int32_t* get_address_of_num_4() { return &___num_4; }
	inline void set_num_4(int32_t value)
	{
		___num_4 = value;
	}

	inline static int32_t get_offset_of_cnt_5() { return static_cast<int32_t>(offsetof(HighScore_t2402950694, ___cnt_5)); }
	inline int32_t get_cnt_5() const { return ___cnt_5; }
	inline int32_t* get_address_of_cnt_5() { return &___cnt_5; }
	inline void set_cnt_5(int32_t value)
	{
		___cnt_5 = value;
	}

	inline static int32_t get_offset_of_index_6() { return static_cast<int32_t>(offsetof(HighScore_t2402950694, ___index_6)); }
	inline int32_t get_index_6() const { return ___index_6; }
	inline int32_t* get_address_of_index_6() { return &___index_6; }
	inline void set_index_6(int32_t value)
	{
		___index_6 = value;
	}

	inline static int32_t get_offset_of_getnum_8() { return static_cast<int32_t>(offsetof(HighScore_t2402950694, ___getnum_8)); }
	inline CharU5BU5D_t1328083999* get_getnum_8() const { return ___getnum_8; }
	inline CharU5BU5D_t1328083999** get_address_of_getnum_8() { return &___getnum_8; }
	inline void set_getnum_8(CharU5BU5D_t1328083999* value)
	{
		___getnum_8 = value;
		Il2CppCodeGenWriteBarrier(&___getnum_8, value);
	}

	inline static int32_t get_offset_of_str_9() { return static_cast<int32_t>(offsetof(HighScore_t2402950694, ___str_9)); }
	inline String_t* get_str_9() const { return ___str_9; }
	inline String_t** get_address_of_str_9() { return &___str_9; }
	inline void set_str_9(String_t* value)
	{
		___str_9 = value;
		Il2CppCodeGenWriteBarrier(&___str_9, value);
	}

	inline static int32_t get_offset_of_pause_10() { return static_cast<int32_t>(offsetof(HighScore_t2402950694, ___pause_10)); }
	inline bool get_pause_10() const { return ___pause_10; }
	inline bool* get_address_of_pause_10() { return &___pause_10; }
	inline void set_pause_10(bool value)
	{
		___pause_10 = value;
	}

	inline static int32_t get_offset_of_ch_12() { return static_cast<int32_t>(offsetof(HighScore_t2402950694, ___ch_12)); }
	inline GameObject_t1756533147 * get_ch_12() const { return ___ch_12; }
	inline GameObject_t1756533147 ** get_address_of_ch_12() { return &___ch_12; }
	inline void set_ch_12(GameObject_t1756533147 * value)
	{
		___ch_12 = value;
		Il2CppCodeGenWriteBarrier(&___ch_12, value);
	}

	inline static int32_t get_offset_of_time_13() { return static_cast<int32_t>(offsetof(HighScore_t2402950694, ___time_13)); }
	inline float get_time_13() const { return ___time_13; }
	inline float* get_address_of_time_13() { return &___time_13; }
	inline void set_time_13(float value)
	{
		___time_13 = value;
	}

	inline static int32_t get_offset_of_m_17() { return static_cast<int32_t>(offsetof(HighScore_t2402950694, ___m_17)); }
	inline int32_t get_m_17() const { return ___m_17; }
	inline int32_t* get_address_of_m_17() { return &___m_17; }
	inline void set_m_17(int32_t value)
	{
		___m_17 = value;
	}

	inline static int32_t get_offset_of_s_18() { return static_cast<int32_t>(offsetof(HighScore_t2402950694, ___s_18)); }
	inline int32_t get_s_18() const { return ___s_18; }
	inline int32_t* get_address_of_s_18() { return &___s_18; }
	inline void set_s_18(int32_t value)
	{
		___s_18 = value;
	}

	inline static int32_t get_offset_of_mstr_19() { return static_cast<int32_t>(offsetof(HighScore_t2402950694, ___mstr_19)); }
	inline String_t* get_mstr_19() const { return ___mstr_19; }
	inline String_t** get_address_of_mstr_19() { return &___mstr_19; }
	inline void set_mstr_19(String_t* value)
	{
		___mstr_19 = value;
		Il2CppCodeGenWriteBarrier(&___mstr_19, value);
	}

	inline static int32_t get_offset_of_sstr_20() { return static_cast<int32_t>(offsetof(HighScore_t2402950694, ___sstr_20)); }
	inline String_t* get_sstr_20() const { return ___sstr_20; }
	inline String_t** get_address_of_sstr_20() { return &___sstr_20; }
	inline void set_sstr_20(String_t* value)
	{
		___sstr_20 = value;
		Il2CppCodeGenWriteBarrier(&___sstr_20, value);
	}

	inline static int32_t get_offset_of_highstr_21() { return static_cast<int32_t>(offsetof(HighScore_t2402950694, ___highstr_21)); }
	inline String_t* get_highstr_21() const { return ___highstr_21; }
	inline String_t** get_address_of_highstr_21() { return &___highstr_21; }
	inline void set_highstr_21(String_t* value)
	{
		___highstr_21 = value;
		Il2CppCodeGenWriteBarrier(&___highstr_21, value);
	}

	inline static int32_t get_offset_of_current_22() { return static_cast<int32_t>(offsetof(HighScore_t2402950694, ___current_22)); }
	inline float get_current_22() const { return ___current_22; }
	inline float* get_address_of_current_22() { return &___current_22; }
	inline void set_current_22(float value)
	{
		___current_22 = value;
	}

	inline static int32_t get_offset_of_minutestr_23() { return static_cast<int32_t>(offsetof(HighScore_t2402950694, ___minutestr_23)); }
	inline String_t* get_minutestr_23() const { return ___minutestr_23; }
	inline String_t** get_address_of_minutestr_23() { return &___minutestr_23; }
	inline void set_minutestr_23(String_t* value)
	{
		___minutestr_23 = value;
		Il2CppCodeGenWriteBarrier(&___minutestr_23, value);
	}

	inline static int32_t get_offset_of_secondstr_24() { return static_cast<int32_t>(offsetof(HighScore_t2402950694, ___secondstr_24)); }
	inline String_t* get_secondstr_24() const { return ___secondstr_24; }
	inline String_t** get_address_of_secondstr_24() { return &___secondstr_24; }
	inline void set_secondstr_24(String_t* value)
	{
		___secondstr_24 = value;
		Il2CppCodeGenWriteBarrier(&___secondstr_24, value);
	}

	inline static int32_t get_offset_of_highsc_25() { return static_cast<int32_t>(offsetof(HighScore_t2402950694, ___highsc_25)); }
	inline String_t* get_highsc_25() const { return ___highsc_25; }
	inline String_t** get_address_of_highsc_25() { return &___highsc_25; }
	inline void set_highsc_25(String_t* value)
	{
		___highsc_25 = value;
		Il2CppCodeGenWriteBarrier(&___highsc_25, value);
	}

	inline static int32_t get_offset_of_sixty_26() { return static_cast<int32_t>(offsetof(HighScore_t2402950694, ___sixty_26)); }
	inline int32_t get_sixty_26() const { return ___sixty_26; }
	inline int32_t* get_address_of_sixty_26() { return &___sixty_26; }
	inline void set_sixty_26(int32_t value)
	{
		___sixty_26 = value;
	}

	inline static int32_t get_offset_of_fsixty_27() { return static_cast<int32_t>(offsetof(HighScore_t2402950694, ___fsixty_27)); }
	inline float get_fsixty_27() const { return ___fsixty_27; }
	inline float* get_address_of_fsixty_27() { return &___fsixty_27; }
	inline void set_fsixty_27(float value)
	{
		___fsixty_27 = value;
	}

	inline static int32_t get_offset_of_minute_28() { return static_cast<int32_t>(offsetof(HighScore_t2402950694, ___minute_28)); }
	inline int32_t get_minute_28() const { return ___minute_28; }
	inline int32_t* get_address_of_minute_28() { return &___minute_28; }
	inline void set_minute_28(int32_t value)
	{
		___minute_28 = value;
	}

	inline static int32_t get_offset_of_uiText_29() { return static_cast<int32_t>(offsetof(HighScore_t2402950694, ___uiText_29)); }
	inline Text_t356221433 * get_uiText_29() const { return ___uiText_29; }
	inline Text_t356221433 ** get_address_of_uiText_29() { return &___uiText_29; }
	inline void set_uiText_29(Text_t356221433 * value)
	{
		___uiText_29 = value;
		Il2CppCodeGenWriteBarrier(&___uiText_29, value);
	}
};

struct HighScore_t2402950694_StaticFields
{
public:
	// System.Boolean[] HighScore::flag
	BooleanU5BU5D_t3568034315* ___flag_7;
	// System.Boolean HighScore::finish
	bool ___finish_11;
	// System.Int32 HighScore::score
	int32_t ___score_14;
	// System.Int32 HighScore::high
	int32_t ___high_15;
	// System.Int32 HighScore::gamecnt
	int32_t ___gamecnt_16;
	// System.Int32 HighScore::nh
	int32_t ___nh_30;

public:
	inline static int32_t get_offset_of_flag_7() { return static_cast<int32_t>(offsetof(HighScore_t2402950694_StaticFields, ___flag_7)); }
	inline BooleanU5BU5D_t3568034315* get_flag_7() const { return ___flag_7; }
	inline BooleanU5BU5D_t3568034315** get_address_of_flag_7() { return &___flag_7; }
	inline void set_flag_7(BooleanU5BU5D_t3568034315* value)
	{
		___flag_7 = value;
		Il2CppCodeGenWriteBarrier(&___flag_7, value);
	}

	inline static int32_t get_offset_of_finish_11() { return static_cast<int32_t>(offsetof(HighScore_t2402950694_StaticFields, ___finish_11)); }
	inline bool get_finish_11() const { return ___finish_11; }
	inline bool* get_address_of_finish_11() { return &___finish_11; }
	inline void set_finish_11(bool value)
	{
		___finish_11 = value;
	}

	inline static int32_t get_offset_of_score_14() { return static_cast<int32_t>(offsetof(HighScore_t2402950694_StaticFields, ___score_14)); }
	inline int32_t get_score_14() const { return ___score_14; }
	inline int32_t* get_address_of_score_14() { return &___score_14; }
	inline void set_score_14(int32_t value)
	{
		___score_14 = value;
	}

	inline static int32_t get_offset_of_high_15() { return static_cast<int32_t>(offsetof(HighScore_t2402950694_StaticFields, ___high_15)); }
	inline int32_t get_high_15() const { return ___high_15; }
	inline int32_t* get_address_of_high_15() { return &___high_15; }
	inline void set_high_15(int32_t value)
	{
		___high_15 = value;
	}

	inline static int32_t get_offset_of_gamecnt_16() { return static_cast<int32_t>(offsetof(HighScore_t2402950694_StaticFields, ___gamecnt_16)); }
	inline int32_t get_gamecnt_16() const { return ___gamecnt_16; }
	inline int32_t* get_address_of_gamecnt_16() { return &___gamecnt_16; }
	inline void set_gamecnt_16(int32_t value)
	{
		___gamecnt_16 = value;
	}

	inline static int32_t get_offset_of_nh_30() { return static_cast<int32_t>(offsetof(HighScore_t2402950694_StaticFields, ___nh_30)); }
	inline int32_t get_nh_30() const { return ___nh_30; }
	inline int32_t* get_address_of_nh_30() { return &___nh_30; }
	inline void set_nh_30(int32_t value)
	{
		___nh_30 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
