﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"

// System.Void GlobalCall::.cctor()
extern "C"  void GlobalCall__cctor_m1051184633 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalCall::setScore(System.Int32)
extern "C"  void GlobalCall_setScore_m1324356053 (Il2CppObject * __this /* static, unused */, int32_t ___score0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GlobalCall::getScore()
extern "C"  int32_t GlobalCall_getScore_m1953950224 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalCall::setHigh(System.Int32)
extern "C"  void GlobalCall_setHigh_m735216011 (Il2CppObject * __this /* static, unused */, int32_t ___score0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GlobalCall::getHigh()
extern "C"  int32_t GlobalCall_getHigh_m3144168644 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GlobalCall::setOrigin(System.Int32)
extern "C"  void GlobalCall_setOrigin_m1076086725 (Il2CppObject * __this /* static, unused */, int32_t ___score0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 GlobalCall::getOrigin()
extern "C"  int32_t GlobalCall_getOrigin_m2405469574 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
