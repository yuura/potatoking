﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HighScore
struct HighScore_t2402950694;

#include "codegen/il2cpp-codegen.h"

// System.Void HighScore::.ctor()
extern "C"  void HighScore__ctor_m1595199985 (HighScore_t2402950694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HighScore::.cctor()
extern "C"  void HighScore__cctor_m2596394000 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HighScore::Start()
extern "C"  void HighScore_Start_m3561044161 (HighScore_t2402950694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HighScore::Update()
extern "C"  void HighScore_Update_m2360423300 (HighScore_t2402950694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HighScore::but()
extern "C"  void HighScore_but_m4158015102 (HighScore_t2402950694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HighScore::savescore()
extern "C"  void HighScore_savescore_m461077820 (HighScore_t2402950694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HighScore::Complete()
extern "C"  void HighScore_Complete_m3901898926 (HighScore_t2402950694 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
