﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Object2689449295.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GlobalCall
struct  GlobalCall_t2994192287  : public Il2CppObject
{
public:

public:
};

struct GlobalCall_t2994192287_StaticFields
{
public:
	// System.Int32 GlobalCall::currenttime
	int32_t ___currenttime_0;
	// System.Int32 GlobalCall::besttime
	int32_t ___besttime_1;
	// System.Single GlobalCall::currentBGM
	float ___currentBGM_2;
	// System.Boolean GlobalCall::isprevplay
	bool ___isprevplay_3;

public:
	inline static int32_t get_offset_of_currenttime_0() { return static_cast<int32_t>(offsetof(GlobalCall_t2994192287_StaticFields, ___currenttime_0)); }
	inline int32_t get_currenttime_0() const { return ___currenttime_0; }
	inline int32_t* get_address_of_currenttime_0() { return &___currenttime_0; }
	inline void set_currenttime_0(int32_t value)
	{
		___currenttime_0 = value;
	}

	inline static int32_t get_offset_of_besttime_1() { return static_cast<int32_t>(offsetof(GlobalCall_t2994192287_StaticFields, ___besttime_1)); }
	inline int32_t get_besttime_1() const { return ___besttime_1; }
	inline int32_t* get_address_of_besttime_1() { return &___besttime_1; }
	inline void set_besttime_1(int32_t value)
	{
		___besttime_1 = value;
	}

	inline static int32_t get_offset_of_currentBGM_2() { return static_cast<int32_t>(offsetof(GlobalCall_t2994192287_StaticFields, ___currentBGM_2)); }
	inline float get_currentBGM_2() const { return ___currentBGM_2; }
	inline float* get_address_of_currentBGM_2() { return &___currentBGM_2; }
	inline void set_currentBGM_2(float value)
	{
		___currentBGM_2 = value;
	}

	inline static int32_t get_offset_of_isprevplay_3() { return static_cast<int32_t>(offsetof(GlobalCall_t2994192287_StaticFields, ___isprevplay_3)); }
	inline bool get_isprevplay_3() const { return ___isprevplay_3; }
	inline bool* get_address_of_isprevplay_3() { return &___isprevplay_3; }
	inline void set_isprevplay_3(bool value)
	{
		___isprevplay_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
