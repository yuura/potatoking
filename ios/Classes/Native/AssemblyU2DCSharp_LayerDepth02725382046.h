﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LayerDepth0
struct  LayerDepth0_t2725382046  : public MonoBehaviour_t1158329972
{
public:

public:
};

struct LayerDepth0_t2725382046_StaticFields
{
public:
	// System.Int32 LayerDepth0::guiDepth
	int32_t ___guiDepth_2;

public:
	inline static int32_t get_offset_of_guiDepth_2() { return static_cast<int32_t>(offsetof(LayerDepth0_t2725382046_StaticFields, ___guiDepth_2)); }
	inline int32_t get_guiDepth_2() const { return ___guiDepth_2; }
	inline int32_t* get_address_of_guiDepth_2() { return &___guiDepth_2; }
	inline void set_guiDepth_2(int32_t value)
	{
		___guiDepth_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
