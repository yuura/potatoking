﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameController
struct  GameController_t3607102586  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject GameController::knife
	GameObject_t1756533147 * ___knife_2;
	// System.Single GameController::baseWidth
	float ___baseWidth_3;
	// System.Single GameController::baseHeight
	float ___baseHeight_4;

public:
	inline static int32_t get_offset_of_knife_2() { return static_cast<int32_t>(offsetof(GameController_t3607102586, ___knife_2)); }
	inline GameObject_t1756533147 * get_knife_2() const { return ___knife_2; }
	inline GameObject_t1756533147 ** get_address_of_knife_2() { return &___knife_2; }
	inline void set_knife_2(GameObject_t1756533147 * value)
	{
		___knife_2 = value;
		Il2CppCodeGenWriteBarrier(&___knife_2, value);
	}

	inline static int32_t get_offset_of_baseWidth_3() { return static_cast<int32_t>(offsetof(GameController_t3607102586, ___baseWidth_3)); }
	inline float get_baseWidth_3() const { return ___baseWidth_3; }
	inline float* get_address_of_baseWidth_3() { return &___baseWidth_3; }
	inline void set_baseWidth_3(float value)
	{
		___baseWidth_3 = value;
	}

	inline static int32_t get_offset_of_baseHeight_4() { return static_cast<int32_t>(offsetof(GameController_t3607102586, ___baseHeight_4)); }
	inline float get_baseHeight_4() const { return ___baseHeight_4; }
	inline float* get_address_of_baseHeight_4() { return &___baseHeight_4; }
	inline void set_baseHeight_4(float value)
	{
		___baseHeight_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
