﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AutoFade
struct AutoFade_t3786508579;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "mscorlib_System_String2029220233.h"

// System.Void AutoFade::.ctor()
extern "C"  void AutoFade__ctor_m4174432764 (AutoFade_t3786508579 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AutoFade::.cctor()
extern "C"  void AutoFade__cctor_m395142333 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// AutoFade AutoFade::get_Instance()
extern "C"  AutoFade_t3786508579 * AutoFade_get_Instance_m950941996 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean AutoFade::get_Fading()
extern "C"  bool AutoFade_get_Fading_m3921776080 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AutoFade::Awake()
extern "C"  void AutoFade_Awake_m1767706303 (AutoFade_t3786508579 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AutoFade::DrawQuad(UnityEngine.Color,System.Single)
extern "C"  void AutoFade_DrawQuad_m1804059946 (AutoFade_t3786508579 * __this, Color_t2020392075  ___aColor0, float ___aAlpha1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator AutoFade::Fade(System.Single,System.Single,UnityEngine.Color)
extern "C"  Il2CppObject * AutoFade_Fade_m3240942000 (AutoFade_t3786508579 * __this, float ___aFadeOutTime0, float ___aFadeInTime1, Color_t2020392075  ___aColor2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AutoFade::StartFade(System.Single,System.Single,UnityEngine.Color)
extern "C"  void AutoFade_StartFade_m257239734 (AutoFade_t3786508579 * __this, float ___aFadeOutTime0, float ___aFadeInTime1, Color_t2020392075  ___aColor2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AutoFade::LoadLevel(System.String,System.Single,System.Single,UnityEngine.Color)
extern "C"  void AutoFade_LoadLevel_m1709620906 (Il2CppObject * __this /* static, unused */, String_t* ___aLevelName0, float ___aFadeOutTime1, float ___aFadeInTime2, Color_t2020392075  ___aColor3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AutoFade::LoadLevel(System.Int32,System.Single,System.Single,UnityEngine.Color)
extern "C"  void AutoFade_LoadLevel_m2302776913 (Il2CppObject * __this /* static, unused */, int32_t ___aLevelIndex0, float ___aFadeOutTime1, float ___aFadeInTime2, Color_t2020392075  ___aColor3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
