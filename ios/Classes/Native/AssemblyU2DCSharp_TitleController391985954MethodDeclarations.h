﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TitleController
struct TitleController_t391985954;

#include "codegen/il2cpp-codegen.h"

// System.Void TitleController::.ctor()
extern "C"  void TitleController__ctor_m1390827847 (TitleController_t391985954 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TitleController::Start()
extern "C"  void TitleController_Start_m127862867 (TitleController_t391985954 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TitleController::Update()
extern "C"  void TitleController_Update_m2789011500 (TitleController_t391985954 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TitleController::OnStartButtonClicked()
extern "C"  void TitleController_OnStartButtonClicked_m2541716943 (TitleController_t391985954 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TitleController::OnOptionButtonClicked()
extern "C"  void TitleController_OnOptionButtonClicked_m2709104756 (TitleController_t391985954 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
