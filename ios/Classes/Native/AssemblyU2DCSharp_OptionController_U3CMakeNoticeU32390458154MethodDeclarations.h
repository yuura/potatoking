﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OptionController/<MakeNotice>c__Iterator1
struct U3CMakeNoticeU3Ec__Iterator1_t2390458154;
// System.Object
struct Il2CppObject;

#include "codegen/il2cpp-codegen.h"

// System.Void OptionController/<MakeNotice>c__Iterator1::.ctor()
extern "C"  void U3CMakeNoticeU3Ec__Iterator1__ctor_m3106708969 (U3CMakeNoticeU3Ec__Iterator1_t2390458154 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object OptionController/<MakeNotice>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C"  Il2CppObject * U3CMakeNoticeU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m3561824315 (U3CMakeNoticeU3Ec__Iterator1_t2390458154 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object OptionController/<MakeNotice>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CMakeNoticeU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m3080120787 (U3CMakeNoticeU3Ec__Iterator1_t2390458154 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean OptionController/<MakeNotice>c__Iterator1::MoveNext()
extern "C"  bool U3CMakeNoticeU3Ec__Iterator1_MoveNext_m1396499911 (U3CMakeNoticeU3Ec__Iterator1_t2390458154 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OptionController/<MakeNotice>c__Iterator1::Dispose()
extern "C"  void U3CMakeNoticeU3Ec__Iterator1_Dispose_m264160716 (U3CMakeNoticeU3Ec__Iterator1_t2390458154 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OptionController/<MakeNotice>c__Iterator1::Reset()
extern "C"  void U3CMakeNoticeU3Ec__Iterator1_Reset_m1088937550 (U3CMakeNoticeU3Ec__Iterator1_t2390458154 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
