﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Clear
struct Clear_t4180584003;

#include "codegen/il2cpp-codegen.h"

// System.Void Clear::.ctor()
extern "C"  void Clear__ctor_m3273534932 (Clear_t4180584003 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Clear::Start()
extern "C"  void Clear_Start_m405010312 (Clear_t4180584003 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Clear::Update()
extern "C"  void Clear_Update_m1741830927 (Clear_t4180584003 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
