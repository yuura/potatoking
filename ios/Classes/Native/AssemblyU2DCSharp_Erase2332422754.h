﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.EventSystems.EventSystem
struct EventSystem_t3466835263;
// UnityEngine.LineRenderer
struct LineRenderer_t849157671;
// UnityEngine.UI.Image
struct Image_t2042527209;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Erase
struct  Erase_t2332422754  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.EventSystems.EventSystem Erase::_eventSystem
	EventSystem_t3466835263 * ____eventSystem_2;
	// System.Boolean Erase::flag
	bool ___flag_3;
	// UnityEngine.LineRenderer Erase::line
	LineRenderer_t849157671 * ___line_4;
	// System.Int32 Erase::count
	int32_t ___count_5;
	// UnityEngine.Vector2 Erase::mousePos
	Vector2_t2243707579  ___mousePos_6;
	// UnityEngine.Vector2 Erase::startPos
	Vector2_t2243707579  ___startPos_7;
	// UnityEngine.Vector2 Erase::endPos
	Vector2_t2243707579  ___endPos_8;
	// UnityEngine.UI.Image Erase::potato
	Image_t2042527209 * ___potato_9;
	// System.Single Erase::baseWidth
	float ___baseWidth_10;
	// System.Single Erase::baseHeight
	float ___baseHeight_11;

public:
	inline static int32_t get_offset_of__eventSystem_2() { return static_cast<int32_t>(offsetof(Erase_t2332422754, ____eventSystem_2)); }
	inline EventSystem_t3466835263 * get__eventSystem_2() const { return ____eventSystem_2; }
	inline EventSystem_t3466835263 ** get_address_of__eventSystem_2() { return &____eventSystem_2; }
	inline void set__eventSystem_2(EventSystem_t3466835263 * value)
	{
		____eventSystem_2 = value;
		Il2CppCodeGenWriteBarrier(&____eventSystem_2, value);
	}

	inline static int32_t get_offset_of_flag_3() { return static_cast<int32_t>(offsetof(Erase_t2332422754, ___flag_3)); }
	inline bool get_flag_3() const { return ___flag_3; }
	inline bool* get_address_of_flag_3() { return &___flag_3; }
	inline void set_flag_3(bool value)
	{
		___flag_3 = value;
	}

	inline static int32_t get_offset_of_line_4() { return static_cast<int32_t>(offsetof(Erase_t2332422754, ___line_4)); }
	inline LineRenderer_t849157671 * get_line_4() const { return ___line_4; }
	inline LineRenderer_t849157671 ** get_address_of_line_4() { return &___line_4; }
	inline void set_line_4(LineRenderer_t849157671 * value)
	{
		___line_4 = value;
		Il2CppCodeGenWriteBarrier(&___line_4, value);
	}

	inline static int32_t get_offset_of_count_5() { return static_cast<int32_t>(offsetof(Erase_t2332422754, ___count_5)); }
	inline int32_t get_count_5() const { return ___count_5; }
	inline int32_t* get_address_of_count_5() { return &___count_5; }
	inline void set_count_5(int32_t value)
	{
		___count_5 = value;
	}

	inline static int32_t get_offset_of_mousePos_6() { return static_cast<int32_t>(offsetof(Erase_t2332422754, ___mousePos_6)); }
	inline Vector2_t2243707579  get_mousePos_6() const { return ___mousePos_6; }
	inline Vector2_t2243707579 * get_address_of_mousePos_6() { return &___mousePos_6; }
	inline void set_mousePos_6(Vector2_t2243707579  value)
	{
		___mousePos_6 = value;
	}

	inline static int32_t get_offset_of_startPos_7() { return static_cast<int32_t>(offsetof(Erase_t2332422754, ___startPos_7)); }
	inline Vector2_t2243707579  get_startPos_7() const { return ___startPos_7; }
	inline Vector2_t2243707579 * get_address_of_startPos_7() { return &___startPos_7; }
	inline void set_startPos_7(Vector2_t2243707579  value)
	{
		___startPos_7 = value;
	}

	inline static int32_t get_offset_of_endPos_8() { return static_cast<int32_t>(offsetof(Erase_t2332422754, ___endPos_8)); }
	inline Vector2_t2243707579  get_endPos_8() const { return ___endPos_8; }
	inline Vector2_t2243707579 * get_address_of_endPos_8() { return &___endPos_8; }
	inline void set_endPos_8(Vector2_t2243707579  value)
	{
		___endPos_8 = value;
	}

	inline static int32_t get_offset_of_potato_9() { return static_cast<int32_t>(offsetof(Erase_t2332422754, ___potato_9)); }
	inline Image_t2042527209 * get_potato_9() const { return ___potato_9; }
	inline Image_t2042527209 ** get_address_of_potato_9() { return &___potato_9; }
	inline void set_potato_9(Image_t2042527209 * value)
	{
		___potato_9 = value;
		Il2CppCodeGenWriteBarrier(&___potato_9, value);
	}

	inline static int32_t get_offset_of_baseWidth_10() { return static_cast<int32_t>(offsetof(Erase_t2332422754, ___baseWidth_10)); }
	inline float get_baseWidth_10() const { return ___baseWidth_10; }
	inline float* get_address_of_baseWidth_10() { return &___baseWidth_10; }
	inline void set_baseWidth_10(float value)
	{
		___baseWidth_10 = value;
	}

	inline static int32_t get_offset_of_baseHeight_11() { return static_cast<int32_t>(offsetof(Erase_t2332422754, ___baseHeight_11)); }
	inline float get_baseHeight_11() const { return ___baseHeight_11; }
	inline float* get_address_of_baseHeight_11() { return &___baseHeight_11; }
	inline void set_baseHeight_11(float value)
	{
		___baseHeight_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
