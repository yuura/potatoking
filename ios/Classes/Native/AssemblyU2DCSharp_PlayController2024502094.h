﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Texture2D
struct Texture2D_t3542995729;
// UnityEngine.UI.Button
struct Button_t2872111280;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.Boolean[]
struct BooleanU5BU5D_t3568034315;
// System.Char[]
struct CharU5BU5D_t1328083999;
// UnityEngine.UI.Text
struct Text_t356221433;
// System.String
struct String_t;
// UnityEngine.AudioSource
struct AudioSource_t1135106623;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlayController
struct  PlayController_t2024502094  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Texture2D PlayController::cursorTexture
	Texture2D_t3542995729 * ___cursorTexture_2;
	// System.Boolean PlayController::hotSpotIsCenter
	bool ___hotSpotIsCenter_3;
	// UnityEngine.Vector2 PlayController::adjustHotSpot
	Vector2_t2243707579  ___adjustHotSpot_4;
	// UnityEngine.Vector2 PlayController::hotSpot
	Vector2_t2243707579  ___hotSpot_5;
	// UnityEngine.UI.Button PlayController::title
	Button_t2872111280 * ___title_6;
	// UnityEngine.UI.Button PlayController::back
	Button_t2872111280 * ___back_7;
	// UnityEngine.GameObject PlayController::ch
	GameObject_t1756533147 * ___ch_8;
	// UnityEngine.GameObject PlayController::menu
	GameObject_t1756533147 * ___menu_9;
	// UnityEngine.GameObject PlayController::potato_inside
	GameObject_t1756533147 * ___potato_inside_10;
	// UnityEngine.GameObject PlayController::potato_outside
	GameObject_t1756533147 * ___potato_outside_11;
	// System.Boolean PlayController::pause
	bool ___pause_12;
	// System.Boolean PlayController::finish
	bool ___finish_13;
	// System.Int32 PlayController::i
	int32_t ___i_14;
	// System.Int32 PlayController::j
	int32_t ___j_15;
	// System.Int32 PlayController::num
	int32_t ___num_16;
	// System.Int32 PlayController::cnt
	int32_t ___cnt_17;
	// System.Int32 PlayController::index
	int32_t ___index_18;
	// System.Char[] PlayController::getnum
	CharU5BU5D_t1328083999* ___getnum_20;
	// UnityEngine.UI.Text PlayController::uiText
	Text_t356221433 * ___uiText_22;
	// System.String PlayController::str
	String_t* ___str_23;
	// UnityEngine.AudioSource PlayController::audiosource
	AudioSource_t1135106623 * ___audiosource_24;
	// UnityEngine.GameObject PlayController::on
	GameObject_t1756533147 * ___on_25;
	// UnityEngine.GameObject PlayController::off
	GameObject_t1756533147 * ___off_26;

public:
	inline static int32_t get_offset_of_cursorTexture_2() { return static_cast<int32_t>(offsetof(PlayController_t2024502094, ___cursorTexture_2)); }
	inline Texture2D_t3542995729 * get_cursorTexture_2() const { return ___cursorTexture_2; }
	inline Texture2D_t3542995729 ** get_address_of_cursorTexture_2() { return &___cursorTexture_2; }
	inline void set_cursorTexture_2(Texture2D_t3542995729 * value)
	{
		___cursorTexture_2 = value;
		Il2CppCodeGenWriteBarrier(&___cursorTexture_2, value);
	}

	inline static int32_t get_offset_of_hotSpotIsCenter_3() { return static_cast<int32_t>(offsetof(PlayController_t2024502094, ___hotSpotIsCenter_3)); }
	inline bool get_hotSpotIsCenter_3() const { return ___hotSpotIsCenter_3; }
	inline bool* get_address_of_hotSpotIsCenter_3() { return &___hotSpotIsCenter_3; }
	inline void set_hotSpotIsCenter_3(bool value)
	{
		___hotSpotIsCenter_3 = value;
	}

	inline static int32_t get_offset_of_adjustHotSpot_4() { return static_cast<int32_t>(offsetof(PlayController_t2024502094, ___adjustHotSpot_4)); }
	inline Vector2_t2243707579  get_adjustHotSpot_4() const { return ___adjustHotSpot_4; }
	inline Vector2_t2243707579 * get_address_of_adjustHotSpot_4() { return &___adjustHotSpot_4; }
	inline void set_adjustHotSpot_4(Vector2_t2243707579  value)
	{
		___adjustHotSpot_4 = value;
	}

	inline static int32_t get_offset_of_hotSpot_5() { return static_cast<int32_t>(offsetof(PlayController_t2024502094, ___hotSpot_5)); }
	inline Vector2_t2243707579  get_hotSpot_5() const { return ___hotSpot_5; }
	inline Vector2_t2243707579 * get_address_of_hotSpot_5() { return &___hotSpot_5; }
	inline void set_hotSpot_5(Vector2_t2243707579  value)
	{
		___hotSpot_5 = value;
	}

	inline static int32_t get_offset_of_title_6() { return static_cast<int32_t>(offsetof(PlayController_t2024502094, ___title_6)); }
	inline Button_t2872111280 * get_title_6() const { return ___title_6; }
	inline Button_t2872111280 ** get_address_of_title_6() { return &___title_6; }
	inline void set_title_6(Button_t2872111280 * value)
	{
		___title_6 = value;
		Il2CppCodeGenWriteBarrier(&___title_6, value);
	}

	inline static int32_t get_offset_of_back_7() { return static_cast<int32_t>(offsetof(PlayController_t2024502094, ___back_7)); }
	inline Button_t2872111280 * get_back_7() const { return ___back_7; }
	inline Button_t2872111280 ** get_address_of_back_7() { return &___back_7; }
	inline void set_back_7(Button_t2872111280 * value)
	{
		___back_7 = value;
		Il2CppCodeGenWriteBarrier(&___back_7, value);
	}

	inline static int32_t get_offset_of_ch_8() { return static_cast<int32_t>(offsetof(PlayController_t2024502094, ___ch_8)); }
	inline GameObject_t1756533147 * get_ch_8() const { return ___ch_8; }
	inline GameObject_t1756533147 ** get_address_of_ch_8() { return &___ch_8; }
	inline void set_ch_8(GameObject_t1756533147 * value)
	{
		___ch_8 = value;
		Il2CppCodeGenWriteBarrier(&___ch_8, value);
	}

	inline static int32_t get_offset_of_menu_9() { return static_cast<int32_t>(offsetof(PlayController_t2024502094, ___menu_9)); }
	inline GameObject_t1756533147 * get_menu_9() const { return ___menu_9; }
	inline GameObject_t1756533147 ** get_address_of_menu_9() { return &___menu_9; }
	inline void set_menu_9(GameObject_t1756533147 * value)
	{
		___menu_9 = value;
		Il2CppCodeGenWriteBarrier(&___menu_9, value);
	}

	inline static int32_t get_offset_of_potato_inside_10() { return static_cast<int32_t>(offsetof(PlayController_t2024502094, ___potato_inside_10)); }
	inline GameObject_t1756533147 * get_potato_inside_10() const { return ___potato_inside_10; }
	inline GameObject_t1756533147 ** get_address_of_potato_inside_10() { return &___potato_inside_10; }
	inline void set_potato_inside_10(GameObject_t1756533147 * value)
	{
		___potato_inside_10 = value;
		Il2CppCodeGenWriteBarrier(&___potato_inside_10, value);
	}

	inline static int32_t get_offset_of_potato_outside_11() { return static_cast<int32_t>(offsetof(PlayController_t2024502094, ___potato_outside_11)); }
	inline GameObject_t1756533147 * get_potato_outside_11() const { return ___potato_outside_11; }
	inline GameObject_t1756533147 ** get_address_of_potato_outside_11() { return &___potato_outside_11; }
	inline void set_potato_outside_11(GameObject_t1756533147 * value)
	{
		___potato_outside_11 = value;
		Il2CppCodeGenWriteBarrier(&___potato_outside_11, value);
	}

	inline static int32_t get_offset_of_pause_12() { return static_cast<int32_t>(offsetof(PlayController_t2024502094, ___pause_12)); }
	inline bool get_pause_12() const { return ___pause_12; }
	inline bool* get_address_of_pause_12() { return &___pause_12; }
	inline void set_pause_12(bool value)
	{
		___pause_12 = value;
	}

	inline static int32_t get_offset_of_finish_13() { return static_cast<int32_t>(offsetof(PlayController_t2024502094, ___finish_13)); }
	inline bool get_finish_13() const { return ___finish_13; }
	inline bool* get_address_of_finish_13() { return &___finish_13; }
	inline void set_finish_13(bool value)
	{
		___finish_13 = value;
	}

	inline static int32_t get_offset_of_i_14() { return static_cast<int32_t>(offsetof(PlayController_t2024502094, ___i_14)); }
	inline int32_t get_i_14() const { return ___i_14; }
	inline int32_t* get_address_of_i_14() { return &___i_14; }
	inline void set_i_14(int32_t value)
	{
		___i_14 = value;
	}

	inline static int32_t get_offset_of_j_15() { return static_cast<int32_t>(offsetof(PlayController_t2024502094, ___j_15)); }
	inline int32_t get_j_15() const { return ___j_15; }
	inline int32_t* get_address_of_j_15() { return &___j_15; }
	inline void set_j_15(int32_t value)
	{
		___j_15 = value;
	}

	inline static int32_t get_offset_of_num_16() { return static_cast<int32_t>(offsetof(PlayController_t2024502094, ___num_16)); }
	inline int32_t get_num_16() const { return ___num_16; }
	inline int32_t* get_address_of_num_16() { return &___num_16; }
	inline void set_num_16(int32_t value)
	{
		___num_16 = value;
	}

	inline static int32_t get_offset_of_cnt_17() { return static_cast<int32_t>(offsetof(PlayController_t2024502094, ___cnt_17)); }
	inline int32_t get_cnt_17() const { return ___cnt_17; }
	inline int32_t* get_address_of_cnt_17() { return &___cnt_17; }
	inline void set_cnt_17(int32_t value)
	{
		___cnt_17 = value;
	}

	inline static int32_t get_offset_of_index_18() { return static_cast<int32_t>(offsetof(PlayController_t2024502094, ___index_18)); }
	inline int32_t get_index_18() const { return ___index_18; }
	inline int32_t* get_address_of_index_18() { return &___index_18; }
	inline void set_index_18(int32_t value)
	{
		___index_18 = value;
	}

	inline static int32_t get_offset_of_getnum_20() { return static_cast<int32_t>(offsetof(PlayController_t2024502094, ___getnum_20)); }
	inline CharU5BU5D_t1328083999* get_getnum_20() const { return ___getnum_20; }
	inline CharU5BU5D_t1328083999** get_address_of_getnum_20() { return &___getnum_20; }
	inline void set_getnum_20(CharU5BU5D_t1328083999* value)
	{
		___getnum_20 = value;
		Il2CppCodeGenWriteBarrier(&___getnum_20, value);
	}

	inline static int32_t get_offset_of_uiText_22() { return static_cast<int32_t>(offsetof(PlayController_t2024502094, ___uiText_22)); }
	inline Text_t356221433 * get_uiText_22() const { return ___uiText_22; }
	inline Text_t356221433 ** get_address_of_uiText_22() { return &___uiText_22; }
	inline void set_uiText_22(Text_t356221433 * value)
	{
		___uiText_22 = value;
		Il2CppCodeGenWriteBarrier(&___uiText_22, value);
	}

	inline static int32_t get_offset_of_str_23() { return static_cast<int32_t>(offsetof(PlayController_t2024502094, ___str_23)); }
	inline String_t* get_str_23() const { return ___str_23; }
	inline String_t** get_address_of_str_23() { return &___str_23; }
	inline void set_str_23(String_t* value)
	{
		___str_23 = value;
		Il2CppCodeGenWriteBarrier(&___str_23, value);
	}

	inline static int32_t get_offset_of_audiosource_24() { return static_cast<int32_t>(offsetof(PlayController_t2024502094, ___audiosource_24)); }
	inline AudioSource_t1135106623 * get_audiosource_24() const { return ___audiosource_24; }
	inline AudioSource_t1135106623 ** get_address_of_audiosource_24() { return &___audiosource_24; }
	inline void set_audiosource_24(AudioSource_t1135106623 * value)
	{
		___audiosource_24 = value;
		Il2CppCodeGenWriteBarrier(&___audiosource_24, value);
	}

	inline static int32_t get_offset_of_on_25() { return static_cast<int32_t>(offsetof(PlayController_t2024502094, ___on_25)); }
	inline GameObject_t1756533147 * get_on_25() const { return ___on_25; }
	inline GameObject_t1756533147 ** get_address_of_on_25() { return &___on_25; }
	inline void set_on_25(GameObject_t1756533147 * value)
	{
		___on_25 = value;
		Il2CppCodeGenWriteBarrier(&___on_25, value);
	}

	inline static int32_t get_offset_of_off_26() { return static_cast<int32_t>(offsetof(PlayController_t2024502094, ___off_26)); }
	inline GameObject_t1756533147 * get_off_26() const { return ___off_26; }
	inline GameObject_t1756533147 ** get_address_of_off_26() { return &___off_26; }
	inline void set_off_26(GameObject_t1756533147 * value)
	{
		___off_26 = value;
		Il2CppCodeGenWriteBarrier(&___off_26, value);
	}
};

struct PlayController_t2024502094_StaticFields
{
public:
	// System.Boolean[] PlayController::flag
	BooleanU5BU5D_t3568034315* ___flag_19;
	// System.Single PlayController::time
	float ___time_21;

public:
	inline static int32_t get_offset_of_flag_19() { return static_cast<int32_t>(offsetof(PlayController_t2024502094_StaticFields, ___flag_19)); }
	inline BooleanU5BU5D_t3568034315* get_flag_19() const { return ___flag_19; }
	inline BooleanU5BU5D_t3568034315** get_address_of_flag_19() { return &___flag_19; }
	inline void set_flag_19(BooleanU5BU5D_t3568034315* value)
	{
		___flag_19 = value;
		Il2CppCodeGenWriteBarrier(&___flag_19, value);
	}

	inline static int32_t get_offset_of_time_21() { return static_cast<int32_t>(offsetof(PlayController_t2024502094_StaticFields, ___time_21)); }
	inline float get_time_21() const { return ___time_21; }
	inline float* get_address_of_time_21() { return &___time_21; }
	inline void set_time_21(float value)
	{
		___time_21 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
