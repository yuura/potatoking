﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Erase
struct Erase_t2332422754;

#include "codegen/il2cpp-codegen.h"

// System.Void Erase::.ctor()
extern "C"  void Erase__ctor_m2756617947 (Erase_t2332422754 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Erase::Start()
extern "C"  void Erase_Start_m823459367 (Erase_t2332422754 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Erase::Update()
extern "C"  void Erase_Update_m4159129776 (Erase_t2332422754 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Erase::createLine()
extern "C"  void Erase_createLine_m3264391977 (Erase_t2332422754 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
