﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OptionController
struct OptionController_t3288053769;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;

#include "codegen/il2cpp-codegen.h"

// System.Void OptionController::.ctor()
extern "C"  void OptionController__ctor_m3372235064 (OptionController_t3288053769 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OptionController::Start()
extern "C"  void OptionController_Start_m884063112 (OptionController_t3288053769 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OptionController::OnBackButtonClicked()
extern "C"  void OptionController_OnBackButtonClicked_m312055907 (OptionController_t3288053769 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OptionController::gameSoundStatusChanged(System.Boolean)
extern "C"  void OptionController_gameSoundStatusChanged_m2519406738 (OptionController_t3288053769 * __this, bool ___isclick0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OptionController::OnInitializationButtonClicked()
extern "C"  void OptionController_OnInitializationButtonClicked_m3843892390 (OptionController_t3288053769 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator OptionController::MakeNotice()
extern "C"  Il2CppObject * OptionController_MakeNotice_m3253978076 (OptionController_t3288053769 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
