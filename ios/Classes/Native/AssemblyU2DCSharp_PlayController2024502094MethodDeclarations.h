﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayController
struct PlayController_t2024502094;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayController::.ctor()
extern "C"  void PlayController__ctor_m2087269799 (PlayController_t2024502094 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayController::Start()
extern "C"  void PlayController_Start_m1038701147 (PlayController_t2024502094 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayController::OnVolumeButtonClicked()
extern "C"  void PlayController_OnVolumeButtonClicked_m898487635 (PlayController_t2024502094 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayController::but()
extern "C"  void PlayController_but_m3194425450 (PlayController_t2024502094 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayController::SetTrueVisibleButton()
extern "C"  void PlayController_SetTrueVisibleButton_m978986607 (PlayController_t2024502094 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayController::OnTitleButtonClicked()
extern "C"  void PlayController_OnTitleButtonClicked_m4000853203 (PlayController_t2024502094 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayController::OnOptionButtonClicked()
extern "C"  void PlayController_OnOptionButtonClicked_m1707671980 (PlayController_t2024502094 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayController::OnBackButtonClicked()
extern "C"  void PlayController_OnBackButtonClicked_m1260219122 (PlayController_t2024502094 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayController::OnPauseButtonClicked()
extern "C"  void PlayController_OnPauseButtonClicked_m4120296361 (PlayController_t2024502094 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
