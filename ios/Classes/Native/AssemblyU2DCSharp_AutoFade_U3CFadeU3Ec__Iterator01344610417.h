﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// AutoFade
struct AutoFade_t3786508579;

#include "mscorlib_System_Object2689449295.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AutoFade/<Fade>c__Iterator0
struct  U3CFadeU3Ec__Iterator0_t1344610417  : public Il2CppObject
{
public:
	// System.Single AutoFade/<Fade>c__Iterator0::<t>__0
	float ___U3CtU3E__0_0;
	// System.Single AutoFade/<Fade>c__Iterator0::aFadeOutTime
	float ___aFadeOutTime_1;
	// UnityEngine.Color AutoFade/<Fade>c__Iterator0::aColor
	Color_t2020392075  ___aColor_2;
	// System.Single AutoFade/<Fade>c__Iterator0::aFadeInTime
	float ___aFadeInTime_3;
	// System.Int32 AutoFade/<Fade>c__Iterator0::$PC
	int32_t ___U24PC_4;
	// System.Object AutoFade/<Fade>c__Iterator0::$current
	Il2CppObject * ___U24current_5;
	// System.Single AutoFade/<Fade>c__Iterator0::<$>aFadeOutTime
	float ___U3CU24U3EaFadeOutTime_6;
	// UnityEngine.Color AutoFade/<Fade>c__Iterator0::<$>aColor
	Color_t2020392075  ___U3CU24U3EaColor_7;
	// System.Single AutoFade/<Fade>c__Iterator0::<$>aFadeInTime
	float ___U3CU24U3EaFadeInTime_8;
	// AutoFade AutoFade/<Fade>c__Iterator0::<>f__this
	AutoFade_t3786508579 * ___U3CU3Ef__this_9;

public:
	inline static int32_t get_offset_of_U3CtU3E__0_0() { return static_cast<int32_t>(offsetof(U3CFadeU3Ec__Iterator0_t1344610417, ___U3CtU3E__0_0)); }
	inline float get_U3CtU3E__0_0() const { return ___U3CtU3E__0_0; }
	inline float* get_address_of_U3CtU3E__0_0() { return &___U3CtU3E__0_0; }
	inline void set_U3CtU3E__0_0(float value)
	{
		___U3CtU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_aFadeOutTime_1() { return static_cast<int32_t>(offsetof(U3CFadeU3Ec__Iterator0_t1344610417, ___aFadeOutTime_1)); }
	inline float get_aFadeOutTime_1() const { return ___aFadeOutTime_1; }
	inline float* get_address_of_aFadeOutTime_1() { return &___aFadeOutTime_1; }
	inline void set_aFadeOutTime_1(float value)
	{
		___aFadeOutTime_1 = value;
	}

	inline static int32_t get_offset_of_aColor_2() { return static_cast<int32_t>(offsetof(U3CFadeU3Ec__Iterator0_t1344610417, ___aColor_2)); }
	inline Color_t2020392075  get_aColor_2() const { return ___aColor_2; }
	inline Color_t2020392075 * get_address_of_aColor_2() { return &___aColor_2; }
	inline void set_aColor_2(Color_t2020392075  value)
	{
		___aColor_2 = value;
	}

	inline static int32_t get_offset_of_aFadeInTime_3() { return static_cast<int32_t>(offsetof(U3CFadeU3Ec__Iterator0_t1344610417, ___aFadeInTime_3)); }
	inline float get_aFadeInTime_3() const { return ___aFadeInTime_3; }
	inline float* get_address_of_aFadeInTime_3() { return &___aFadeInTime_3; }
	inline void set_aFadeInTime_3(float value)
	{
		___aFadeInTime_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CFadeU3Ec__Iterator0_t1344610417, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}

	inline static int32_t get_offset_of_U24current_5() { return static_cast<int32_t>(offsetof(U3CFadeU3Ec__Iterator0_t1344610417, ___U24current_5)); }
	inline Il2CppObject * get_U24current_5() const { return ___U24current_5; }
	inline Il2CppObject ** get_address_of_U24current_5() { return &___U24current_5; }
	inline void set_U24current_5(Il2CppObject * value)
	{
		___U24current_5 = value;
		Il2CppCodeGenWriteBarrier(&___U24current_5, value);
	}

	inline static int32_t get_offset_of_U3CU24U3EaFadeOutTime_6() { return static_cast<int32_t>(offsetof(U3CFadeU3Ec__Iterator0_t1344610417, ___U3CU24U3EaFadeOutTime_6)); }
	inline float get_U3CU24U3EaFadeOutTime_6() const { return ___U3CU24U3EaFadeOutTime_6; }
	inline float* get_address_of_U3CU24U3EaFadeOutTime_6() { return &___U3CU24U3EaFadeOutTime_6; }
	inline void set_U3CU24U3EaFadeOutTime_6(float value)
	{
		___U3CU24U3EaFadeOutTime_6 = value;
	}

	inline static int32_t get_offset_of_U3CU24U3EaColor_7() { return static_cast<int32_t>(offsetof(U3CFadeU3Ec__Iterator0_t1344610417, ___U3CU24U3EaColor_7)); }
	inline Color_t2020392075  get_U3CU24U3EaColor_7() const { return ___U3CU24U3EaColor_7; }
	inline Color_t2020392075 * get_address_of_U3CU24U3EaColor_7() { return &___U3CU24U3EaColor_7; }
	inline void set_U3CU24U3EaColor_7(Color_t2020392075  value)
	{
		___U3CU24U3EaColor_7 = value;
	}

	inline static int32_t get_offset_of_U3CU24U3EaFadeInTime_8() { return static_cast<int32_t>(offsetof(U3CFadeU3Ec__Iterator0_t1344610417, ___U3CU24U3EaFadeInTime_8)); }
	inline float get_U3CU24U3EaFadeInTime_8() const { return ___U3CU24U3EaFadeInTime_8; }
	inline float* get_address_of_U3CU24U3EaFadeInTime_8() { return &___U3CU24U3EaFadeInTime_8; }
	inline void set_U3CU24U3EaFadeInTime_8(float value)
	{
		___U3CU24U3EaFadeInTime_8 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__this_9() { return static_cast<int32_t>(offsetof(U3CFadeU3Ec__Iterator0_t1344610417, ___U3CU3Ef__this_9)); }
	inline AutoFade_t3786508579 * get_U3CU3Ef__this_9() const { return ___U3CU3Ef__this_9; }
	inline AutoFade_t3786508579 ** get_address_of_U3CU3Ef__this_9() { return &___U3CU3Ef__this_9; }
	inline void set_U3CU3Ef__this_9(AutoFade_t3786508579 * value)
	{
		___U3CU3Ef__this_9 = value;
		Il2CppCodeGenWriteBarrier(&___U3CU3Ef__this_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
