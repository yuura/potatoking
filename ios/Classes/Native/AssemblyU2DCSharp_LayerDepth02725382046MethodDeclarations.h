﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LayerDepth0
struct LayerDepth0_t2725382046;

#include "codegen/il2cpp-codegen.h"

// System.Void LayerDepth0::.ctor()
extern "C"  void LayerDepth0__ctor_m3814776793 (LayerDepth0_t2725382046 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LayerDepth0::.cctor()
extern "C"  void LayerDepth0__cctor_m3681612936 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LayerDepth0::OnGUI()
extern "C"  void LayerDepth0_OnGUI_m3795493627 (LayerDepth0_t2725382046 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
