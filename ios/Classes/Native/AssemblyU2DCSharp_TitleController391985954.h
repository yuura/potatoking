﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.AudioSource
struct AudioSource_t1135106623;
// UnityEngine.UI.Text
struct Text_t356221433;
// System.String
struct String_t;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TitleController
struct  TitleController_t391985954  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.AudioSource TitleController::bgm
	AudioSource_t1135106623 * ___bgm_2;
	// UnityEngine.AudioSource TitleController::audiosource
	AudioSource_t1135106623 * ___audiosource_3;
	// UnityEngine.UI.Text TitleController::record
	Text_t356221433 * ___record_4;
	// System.Int32 TitleController::result
	int32_t ___result_5;
	// System.String TitleController::mstr
	String_t* ___mstr_6;
	// System.String TitleController::sstr
	String_t* ___sstr_7;
	// System.Int32 TitleController::m
	int32_t ___m_8;
	// System.Int32 TitleController::s
	int32_t ___s_9;

public:
	inline static int32_t get_offset_of_bgm_2() { return static_cast<int32_t>(offsetof(TitleController_t391985954, ___bgm_2)); }
	inline AudioSource_t1135106623 * get_bgm_2() const { return ___bgm_2; }
	inline AudioSource_t1135106623 ** get_address_of_bgm_2() { return &___bgm_2; }
	inline void set_bgm_2(AudioSource_t1135106623 * value)
	{
		___bgm_2 = value;
		Il2CppCodeGenWriteBarrier(&___bgm_2, value);
	}

	inline static int32_t get_offset_of_audiosource_3() { return static_cast<int32_t>(offsetof(TitleController_t391985954, ___audiosource_3)); }
	inline AudioSource_t1135106623 * get_audiosource_3() const { return ___audiosource_3; }
	inline AudioSource_t1135106623 ** get_address_of_audiosource_3() { return &___audiosource_3; }
	inline void set_audiosource_3(AudioSource_t1135106623 * value)
	{
		___audiosource_3 = value;
		Il2CppCodeGenWriteBarrier(&___audiosource_3, value);
	}

	inline static int32_t get_offset_of_record_4() { return static_cast<int32_t>(offsetof(TitleController_t391985954, ___record_4)); }
	inline Text_t356221433 * get_record_4() const { return ___record_4; }
	inline Text_t356221433 ** get_address_of_record_4() { return &___record_4; }
	inline void set_record_4(Text_t356221433 * value)
	{
		___record_4 = value;
		Il2CppCodeGenWriteBarrier(&___record_4, value);
	}

	inline static int32_t get_offset_of_result_5() { return static_cast<int32_t>(offsetof(TitleController_t391985954, ___result_5)); }
	inline int32_t get_result_5() const { return ___result_5; }
	inline int32_t* get_address_of_result_5() { return &___result_5; }
	inline void set_result_5(int32_t value)
	{
		___result_5 = value;
	}

	inline static int32_t get_offset_of_mstr_6() { return static_cast<int32_t>(offsetof(TitleController_t391985954, ___mstr_6)); }
	inline String_t* get_mstr_6() const { return ___mstr_6; }
	inline String_t** get_address_of_mstr_6() { return &___mstr_6; }
	inline void set_mstr_6(String_t* value)
	{
		___mstr_6 = value;
		Il2CppCodeGenWriteBarrier(&___mstr_6, value);
	}

	inline static int32_t get_offset_of_sstr_7() { return static_cast<int32_t>(offsetof(TitleController_t391985954, ___sstr_7)); }
	inline String_t* get_sstr_7() const { return ___sstr_7; }
	inline String_t** get_address_of_sstr_7() { return &___sstr_7; }
	inline void set_sstr_7(String_t* value)
	{
		___sstr_7 = value;
		Il2CppCodeGenWriteBarrier(&___sstr_7, value);
	}

	inline static int32_t get_offset_of_m_8() { return static_cast<int32_t>(offsetof(TitleController_t391985954, ___m_8)); }
	inline int32_t get_m_8() const { return ___m_8; }
	inline int32_t* get_address_of_m_8() { return &___m_8; }
	inline void set_m_8(int32_t value)
	{
		___m_8 = value;
	}

	inline static int32_t get_offset_of_s_9() { return static_cast<int32_t>(offsetof(TitleController_t391985954, ___s_9)); }
	inline int32_t get_s_9() const { return ___s_9; }
	inline int32_t* get_address_of_s_9() { return &___s_9; }
	inline void set_s_9(int32_t value)
	{
		___s_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
