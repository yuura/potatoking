﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.Toggle
struct Toggle_t3976754468;
// UnityEngine.AudioSource
struct AudioSource_t1135106623;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OptionController
struct  OptionController_t3288053769  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.GameObject OptionController::notice
	GameObject_t1756533147 * ___notice_2;
	// UnityEngine.UI.Toggle OptionController::currenttoggle
	Toggle_t3976754468 * ___currenttoggle_3;
	// UnityEngine.AudioSource OptionController::audiosource
	AudioSource_t1135106623 * ___audiosource_4;

public:
	inline static int32_t get_offset_of_notice_2() { return static_cast<int32_t>(offsetof(OptionController_t3288053769, ___notice_2)); }
	inline GameObject_t1756533147 * get_notice_2() const { return ___notice_2; }
	inline GameObject_t1756533147 ** get_address_of_notice_2() { return &___notice_2; }
	inline void set_notice_2(GameObject_t1756533147 * value)
	{
		___notice_2 = value;
		Il2CppCodeGenWriteBarrier(&___notice_2, value);
	}

	inline static int32_t get_offset_of_currenttoggle_3() { return static_cast<int32_t>(offsetof(OptionController_t3288053769, ___currenttoggle_3)); }
	inline Toggle_t3976754468 * get_currenttoggle_3() const { return ___currenttoggle_3; }
	inline Toggle_t3976754468 ** get_address_of_currenttoggle_3() { return &___currenttoggle_3; }
	inline void set_currenttoggle_3(Toggle_t3976754468 * value)
	{
		___currenttoggle_3 = value;
		Il2CppCodeGenWriteBarrier(&___currenttoggle_3, value);
	}

	inline static int32_t get_offset_of_audiosource_4() { return static_cast<int32_t>(offsetof(OptionController_t3288053769, ___audiosource_4)); }
	inline AudioSource_t1135106623 * get_audiosource_4() const { return ___audiosource_4; }
	inline AudioSource_t1135106623 ** get_address_of_audiosource_4() { return &___audiosource_4; }
	inline void set_audiosource_4(AudioSource_t1135106623 * value)
	{
		___audiosource_4 = value;
		Il2CppCodeGenWriteBarrier(&___audiosource_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
