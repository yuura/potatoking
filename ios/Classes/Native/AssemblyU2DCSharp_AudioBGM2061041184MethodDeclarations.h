﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AudioBGM
struct AudioBGM_t2061041184;

#include "codegen/il2cpp-codegen.h"

// System.Void AudioBGM::.ctor()
extern "C"  void AudioBGM__ctor_m1658509437 (AudioBGM_t2061041184 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
