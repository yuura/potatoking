﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LayerDepth1
struct LayerDepth1_t2725382045;

#include "codegen/il2cpp-codegen.h"

// System.Void LayerDepth1::.ctor()
extern "C"  void LayerDepth1__ctor_m3811290904 (LayerDepth1_t2725382045 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LayerDepth1::.cctor()
extern "C"  void LayerDepth1__cctor_m3566580839 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LayerDepth1::OnGUI()
extern "C"  void LayerDepth1_OnGUI_m3792012028 (LayerDepth1_t2725382045 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
