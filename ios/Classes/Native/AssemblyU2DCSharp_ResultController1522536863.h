﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.GameObject
struct GameObject_t1756533147;

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ResultController
struct  ResultController_t1522536863  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 ResultController::score
	int32_t ___score_2;
	// System.String ResultController::mstr
	String_t* ___mstr_3;
	// System.String ResultController::sstr
	String_t* ___sstr_4;
	// System.Int32 ResultController::m
	int32_t ___m_5;
	// System.Int32 ResultController::s
	int32_t ___s_6;
	// UnityEngine.UI.Text ResultController::text
	Text_t356221433 * ___text_7;
	// UnityEngine.GameObject ResultController::normal
	GameObject_t1756533147 * ___normal_8;
	// UnityEngine.GameObject ResultController::best
	GameObject_t1756533147 * ___best_9;

public:
	inline static int32_t get_offset_of_score_2() { return static_cast<int32_t>(offsetof(ResultController_t1522536863, ___score_2)); }
	inline int32_t get_score_2() const { return ___score_2; }
	inline int32_t* get_address_of_score_2() { return &___score_2; }
	inline void set_score_2(int32_t value)
	{
		___score_2 = value;
	}

	inline static int32_t get_offset_of_mstr_3() { return static_cast<int32_t>(offsetof(ResultController_t1522536863, ___mstr_3)); }
	inline String_t* get_mstr_3() const { return ___mstr_3; }
	inline String_t** get_address_of_mstr_3() { return &___mstr_3; }
	inline void set_mstr_3(String_t* value)
	{
		___mstr_3 = value;
		Il2CppCodeGenWriteBarrier(&___mstr_3, value);
	}

	inline static int32_t get_offset_of_sstr_4() { return static_cast<int32_t>(offsetof(ResultController_t1522536863, ___sstr_4)); }
	inline String_t* get_sstr_4() const { return ___sstr_4; }
	inline String_t** get_address_of_sstr_4() { return &___sstr_4; }
	inline void set_sstr_4(String_t* value)
	{
		___sstr_4 = value;
		Il2CppCodeGenWriteBarrier(&___sstr_4, value);
	}

	inline static int32_t get_offset_of_m_5() { return static_cast<int32_t>(offsetof(ResultController_t1522536863, ___m_5)); }
	inline int32_t get_m_5() const { return ___m_5; }
	inline int32_t* get_address_of_m_5() { return &___m_5; }
	inline void set_m_5(int32_t value)
	{
		___m_5 = value;
	}

	inline static int32_t get_offset_of_s_6() { return static_cast<int32_t>(offsetof(ResultController_t1522536863, ___s_6)); }
	inline int32_t get_s_6() const { return ___s_6; }
	inline int32_t* get_address_of_s_6() { return &___s_6; }
	inline void set_s_6(int32_t value)
	{
		___s_6 = value;
	}

	inline static int32_t get_offset_of_text_7() { return static_cast<int32_t>(offsetof(ResultController_t1522536863, ___text_7)); }
	inline Text_t356221433 * get_text_7() const { return ___text_7; }
	inline Text_t356221433 ** get_address_of_text_7() { return &___text_7; }
	inline void set_text_7(Text_t356221433 * value)
	{
		___text_7 = value;
		Il2CppCodeGenWriteBarrier(&___text_7, value);
	}

	inline static int32_t get_offset_of_normal_8() { return static_cast<int32_t>(offsetof(ResultController_t1522536863, ___normal_8)); }
	inline GameObject_t1756533147 * get_normal_8() const { return ___normal_8; }
	inline GameObject_t1756533147 ** get_address_of_normal_8() { return &___normal_8; }
	inline void set_normal_8(GameObject_t1756533147 * value)
	{
		___normal_8 = value;
		Il2CppCodeGenWriteBarrier(&___normal_8, value);
	}

	inline static int32_t get_offset_of_best_9() { return static_cast<int32_t>(offsetof(ResultController_t1522536863, ___best_9)); }
	inline GameObject_t1756533147 * get_best_9() const { return ___best_9; }
	inline GameObject_t1756533147 ** get_address_of_best_9() { return &___best_9; }
	inline void set_best_9(GameObject_t1756533147 * value)
	{
		___best_9 = value;
		Il2CppCodeGenWriteBarrier(&___best_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
